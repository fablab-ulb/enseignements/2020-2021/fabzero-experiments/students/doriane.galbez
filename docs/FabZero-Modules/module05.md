# 5. Electronique 1 - Prototypage

Cette semaine nous avons travaillé avec des composants électroniques ainsi qu'un microcontrôleur (plus précisément une carte Arduino Uno) afin de réaliser des circuits.
Un microcontrôleur est une carte électronique rassemblant divers éléments tels qu'un microprocesseur, une mémoire, des unités périfériques et des interfaces d'entrées/sorties. Une carte Arduino est une carte programmable à laquelle nous pouvons connecter divers composants.

## Outils

Arduino propose une interface de développement permettant de programmer ses cartes en utilisant le langage c. En complément de la carte Arduino Uno, nous avions à notre disposition un ensemble de divers composants et capteurs. Enfin, pour montrer les branchements de mes circuits, j'ai réaliser des schémas à l'aide du site de Tinkercad. Ce dernier permet également de simuler des circuits, ce qui peut être très intéressant pour tester un circuit si vous n'avez pas à votre disposition le matériel.


* Interface de développement : [IDE Arduino](https://www.arduino.cc/en/software/)
* Micrcontrôleur : [carte Arduino Uno](https://fr.wikipedia.org/wiki/Arduino)
* Pour faire les schémas : [Tinkercad](https://www.tinkercad.com/)

## Méthodologie

Lorsque je développe un circuit, je commence toujours par étudier chacun des composants que je souhaite utiliser. Pour cela, je fais un circuit pour chaque composants indépendemment des autres, puis j'assemble au progressivement les composants entres eux. Cette méthode me permet d'apprendre à utiliser les composants et aussi de vérifier qu'il sont opérationnel ! En effet, en électronique il peut arriver qu'un composant soit hors d'usage. C'est pour cela que je fais toujours un circuit pour véfier que le composant est fonctionnel. Sans cela, si je ne faisais pas de test et que mon circuit ne fonctionnais pas, je ne pourais pas savoir d'où viendrait mon problème : du composant, du code, du branchement... Grâce au test je m'assure que je sais correctement brancher mon composant et qu'il est fonctionnel ce qui me facilitera la tâche pour le débuggage ensuite.

Pour réaliser un circuit de test de mon composant, je commance par en chercher la datasheet. La datasheet d'un composant est un document (souvent pdf) dans lequel on trouve les spécifications d'un composant. En général, on peut y trouver les valeurs maximum de tension et d'intensité pouvant être supportés (ne pas respecter ces valeurs risque grandement d'endomager le composant), un schéma du circuit électronique (indiquant alors l'utilisé de chaque connecteur) et les capacités du composant. J'utilise donc la datasheet pour établir la tension d'alimentation et les connexions. Pour trouver la datasheet d'un composant en général il suffit de faire la recherche internet : "référence du composant + datasheet".

Pour rendre le code plus facil à comprendre et aussi plus facil à modifier ou réutiliser, je conseil d'utiliser des fonctions.

Lorsqu'on développe un circuit on a souvent envie de se lancer directement. Mais je conseille vraiment d'utiliser la méthode pas à pas et l'utilisation de fonctions. Cela vous permettra de limiter le risque d'erreur (et en électronique les erreurs ça arrive vite) et de bien comprendre ce que vous faites. Comme ça vous pourrez facilement réutiliser des éléments de vortre circuits.

Par exemple, dans mon programme 2. Pour afficher un segment de mon afficheur 7-segments j'utilise une fonction. Dans mon programme 3, je souhaite aussi pouvoir afficher un segment de l'afficheur. Je n'ai alors qu'à simplement reprendre ma fonction et dans ma fonction principale (_void loop()_) j'appelle ma fonction "affiche_un_seg". Dans ma fonction principale je n'ai alors plus qu'une ligne et je sais directement ce qu'elle fait, elle affiche un segment.
C'est un peu fastidieux d'écrire les fonctions au début, mais par la suite cela vous fera gagner beaucoup de temps !


## Outils de développement
### Interface de développement

Comme dit précédemment, Arduino propose un inteface de développement (IDE) : [IDE Arduino](https://www.arduino.cc/en/software/). Cette interface permet d'écrire du code en langage c, de traduire ce code pour la carte et de l'envoyer sur la carte Arduino.

Le langage c a été conçu pour faciliter la programmation. Ce langage n'est pas directement compréhensible par la carte. Il faut donc le traduire en un code exécutable par la carte. Et grâce à l'IDE, c'est possible de le faire en un seul clic ! ;)

En premier lieu, il faut sélectionné le type de votre carte et le port de votre PC où vous l'avez connectée. Pour cela, il faut aller dans _Outils -> Type de carte : xxx"_ puis sélectionné la carte que vous utilisé. Dans ce module nous avons travaillé avec la carte Arduino Uno c'est donc celle-ci que j'ai sélectionnée. Ensuite, toujours dans l'onglet _Outils_ allez dans _Port_ et sélectionné celui avec votre carte entre parenthèse. Cette IDE est pratique pour ça entre autre, elle vous aide à trouver le bon port où est votre carte. Il est important de préciser le type de carte carte toutes les cartes n'ont pas les mêmes spécificités. En effet, certaines instructions (c'est-à-dire une ligne de code) ne peuvent pas être interprêtés (comprises) par certain type de carte car toutes les cartes n'utilisent pas les mêmes microprocesseur et n'ont pas les mêmes capacités. Il est également important de préciser le port où est connecté votre carte car cela permet au logiciel de savoir où envoyer votre code.


Dans l'IDE Arduino lorsque vous cliquez sur "Vérifier", votre programme est compilé (c'est-à-dire traduit pour être exécuté sur votre carte).

![](../images/ide_arduino_verifier.JPG)

La compilation vous permettra de vérifier si votre code comporte des erreurs. Le résultat de la compilation est affiché en bas de la fenêtre. Si vous avez plusieurs erreurs il ne faut pas forcément ne regarder que la dernière d'affichée. En effet, il peut arriver (et c'est même courant) qu'une erreur affichée soit en réalité causée par une erreur située plus haut dans les lignes de codes. En général, il vaut mieux commencer par regarder la première erreur que le compilateur a trouvée.

>Petit conseil :
>Il peut arriver que vous ayez une erreur indiquée sur une ligne où il n'y a pourtant pas d'erreur apparante, par exemple "int i = 1;". Dans ces cas là, il faut regarder les lignes précédentes, l'erreur provient certainement d'un ";" oublié ou d'une parenthèse (accolade ou crochet) fermante manquante.

Après avoir vérifié votre code, vous pouvez l'envoyer sur votre carte, c'est-à-dire le téléverser.

![](../images/ide_arduino_televerser.JPG)

Et voilà ! Maintenant votre programme sera exécuté par votre carte dès qu'elle qu'elle sera alimenté par votre une tension électrique. Lorsque votre catre est branchée sur un ordinateur elle est alimenté. Donc lorsque vous téléversé votre programme comme la carte est branchée à l'ordinateur, elle est alimentée et elle exécute votre programme immédiatement. Le code que vous avez téléversé reste sur la carte tant que vous n'en mettez pas un autre. Il est donc possible d'alimenté la carte par une source extérieure ce qui permet d'avoir un système indépendant d'un ordinateur et donc transpostable. C'est d'ailleur là l'intéret de ce genre de carte, c'est de pouvoir faire des systèmes embarqués !


### Plaquette de développement

Les plaquettes de développement (aussi appelées plaquettes sk10 ou breadbord) sont un outil très pratique pour réaliser des maquettes de circuit électronique. Cette plaquette permet de connecter des fils (et des composants) entre eux. Sur ces plaquettes il y a deux parties : les deux grandes lignes en haut et en bas et les colonnes au milieu. Très souvent, à côté des lignes, il y a un "-" et un "+" et à côté des colonnes, il y a des lettres allant de A à J.

Pour les grandes lignes en haut et en bas, les fils branchés sur une même ligne sont connectés entre eux. Ainsi, tous les fils connectés par exemple à la première ligne sont connectés les uns aux autres.

![](../images/breadboard_ligne_alim.JPG)
![](../images/breadboard_ligne_masse.JPG)

En général, on utilise la ligne marquée du "+" pour connecter l'alimentation et la ligne marquée du "-" pour connecter la masse. La masse correspond à une tension de 0V. Sans la masse ne courant ne circulerait pas, il ne faut donc pas l'oublier ! Sur la carte Arduino, la masse est appelée _GND_ (ground en anglais).


Pour les colonnes du milieu, les fils sont connectés entre eux lorsqu'ils sont branchés sur la même colonne. Les colonnes vont seulement de la ligne A à la ligne E et de la ligne F à la ligne J. Un fil branché sur la ligne B sera donc connecté aux lignes A, C, D et E de sa colonne mais pas aux lignes F à J de cette même colonne.

![](../images/breadboard_col1.JPG)

Voici ci-dessous l'exemple d'un montage qui consiste simplement à allumer des lampes :

![](../images/breadboard_lampes1.JPG)

>Note :
>Les piles AA délivrent une tension de 1,5V. Mais si vous en placez deux en série (le côté - de la première en contacte avec le côté + de la deuxième) comme c'est le cas sur le schéma précédent, leurs tensions s'ajoutent l'une à l'autre. Vous obtenez ainsi une source d'alimentation de 3V. Vous pouvez utiliser ce principe pour obtenir la source d'alimentation que vous voulez (1,5V, 3V, 4,5V, 6V, etc.).

## Programmes et montages

### Structure du programme

On peut diviser la structure du programme en quatre parties :  

* la première commançant à la première ligne et s'arrêtant à la fonction _voit setup()_
* la deuxième étant la fonction _void setup()_
* la troisième étant la fonction _void loop()_
* la quartième commançant après la fonction _void loop()_ et allant jusqu'à la fin du programme.

Dans la première partie, on définit les éléments qui seront utilisable dans tout le code. Pour rappel, en c les variables sont locales aux fonctions (elles ne sont définis que dans les fonctions où elles sont créées). Si on veut qu'une variable sont utilisable par toutes les fonctions ont peut créer une variable dite globale en la créant au début du code avant la première fonction. Dans cette première partie il faut égamement inclure les bibliothèques que vous utiliserez dans le reste du code. Il n'est pas obligatoire d'utiliser des bibliothèques, cependant elles sont très nombreuses pour Arduino et vous permettrons souvent d'utiliser facilement certains composants.

La fonction _void setup()_ est la fonction qui sert d'initialisation à votre programme. Il faut y préciser la fonction entrée ou sortie de vos ports (les ports vont de 0 à 13 pour les ports digitals et de A0 à A5 pour les ports analogiques). L'initialisation d'un port en tant qu'entrée se fait par la commande : ```pinMode(numéro_du_port,INPUT)```. L'initialisation d'un port en tant que sortie se fait par la commande : ```pinMode(numéro_du_port,OUTPUT)```. Par exemple pour initialiser une LED (il s'agit d'une sortie car nous souhaitons la piloter et non lire sa valeur) connectée sur le port 8, il faut écrite ```pinMode(8,OUTPUT)```.

Ensuite, vient la fonction _void loop()_. Cette fonction est le coeur du programme. Elle sera répétée en boucle tant que la carte sera alimentée. C'est ici qu'il faut écrire les instructions qui décivent ce que vous voulez faire.

Enfin, après la fonction _void loop()_ vous pouvez placer ici les fonctions que vous voulez créer. Ces fonctions peuvent être appelées dans la fonction _loop()_ juste avant.

![](../images/organisation_code.JPG)

>Conseil :
>Je conseil d'utiliser des variables globales (en utilisant des ```const int NOM = x``` ou ```#define NOM x```) pour nommer vos numéros de ports et d'ensuite appeler la variable dans le reste du code plutôt que d'écrire directement le numéro du port à plusieurs reprise dans le code. Cela vous permettra de changer le numéro du port facilement et rapidement. En effet, avec cette technique il n'y a qu'un seul numéro au début du code à changer au lieu de devoir le changer à plusieurs endroits dans votre code et de risquer d'en oublier un et d'avoir une erreur. Cela permet aussi de savoir à quoi correspond ce numéro de port.

>Remarque :
>Dans mes programmes, j'écrit toujours mes variables globales en majuscule. Cela me permet de les repérer facilement dans le code ensuite.


### Programme/circuit 1

L'objectif de ce premier programme est de faire clignoter une LED et de contrôler la fréquence de clignotement avec un potentiomètre.

#### Vérification de la LED
Pour vérifier que la LED fonctionne bien on peut utiliser un des programmes d'exemple disponible dans l'IDE Arduino. Pour la LED l'exemple _Blink_ est idéal. Ce dernier permet de faire clignoter un LED avec une fréquence de 1 seconde.

![](../images/ide_arduino_blink.png)

```
/*
  Blink

  Turns an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO
  it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to
  the correct LED pin independent of which board is used.
  If you want to know what pin the on-board LED is connected to on your Arduino
  model, check the Technical Specs of your board at:
  https://www.arduino.cc/en/Main/Products

  modified 8 May 2014
  by Scott Fitzgerald
  modified 2 Sep 2016
  by Arturo Guadalupi
  modified 8 Sep 2016
  by Colby Newman

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Blink
*/

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```

Dans cet exemple, le branchement de la LED est désigné par _LED_BUILTIN_. Pour que votre branchement soit correctement pris en compte il vous suffit de remplacer cette variable interne par le numéro du port sur lequel est branché votre LED. Là je conseil de remplacer _LED_BUILTIN_ par une variable telle que _LED_ par exemple et d'ajouter l'instruction ```const int LED = 7;``` (si la LED est connectée au port 7). Comme cela, si vous voulez brancher la LED à un autre port il n'y aura que cette valeur à modifier.

On peut noter que la LED est initialisée dans le _void setup()_ par ```pinMode(LED,OUTPUT);```. Il s'agit donc d'une sortie. En effet, nous utilisons la carte Arduino pour piloter la LED et nous ne recevons pas de données. La LED est bien une sortie.


#### Vérification du potentiomètre

Le deuxième élément du mon circuit est le potentiomètre, je vais donc comme pour la LED le vérifier.

Le potentiomètre possède une résistance que l'on peut faire varier. Faire varier cette résistance permet de faire varier la tension en sortie du potentiomètre. C'est cette tension qui est lue par la carte Arduino.

On peut alors réprésenter le potentiomètre de la manière suivante :

![](../images/schema_poto2.JPG)

Contrairement à la LED, le potentiomètre est une entrée (donc ```pinMode(POTO,OUTPUT)```). Il est possible de lire la valeur du potentiomètre et d'ensuite l'afficher dans le moniteur série. On peut afficher du texte ou des valeurs dans le moniteur série en utilisant ```Serial.out.print``` ou ```Serial.out.println```. On peut ouvrir le moniteur série en cliquant sur le petit icone en haut à droite dans l'IDE. _(Remarque : on ne peut ouvrir le moniteur série que lorsque le programme est en train de touner sur une carte, sinon le moniteur ne saurait pas quoi afficher.)_

![](../images/ide_arduino_moniteur.JPG)

Attention ! Lorsqu'on utilise le moniteur série, il faut initialiser la communication ! Il ne faut donc pas l'oublier dans le setup. Initialiser la communication série se fait avec une simple ligne : ```Serial.begin(9600)```. Cette instruction permet de commencer la communication série en définissant sa vitesse à 9600 bauds.
_Vous trouverez plus d'information sur cette instructions [ici](https://www.arduino.cc/reference/tr/language/functions/communication/serial/begin/)._

Pour tester le potentiomètre, j'ai alors écrit le code suivant :

```
/*
 *
 *  File    : prog_test_poto.ino
 *  Author  : Doriane Galbez
 *  Date    : 11/03/2021
 *  License : This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 *
 *
 * Programme lisant la valeur d'un potentiometre
 *   - potentiometre connecte au port A0
 *
 *
 */

//definition des noms attribues aux ports
const int POTO = A0;  //potentiometre connecte sur le port A0
int var;

void setup() {
  Serial.begin(9600);     //initialise la communication serie
  pinMode(POTO,INPUT);    //initialisation du port en tant qu'entree
}

void loop() {
  var = analogRead(POTO); //lit la valeur du potentiometre
  Serial.out.println(var); //affiche la valeur du potentiometre dans le moniteur serie
  delay(1000);    //attent une seconde
}
```

>A noter :
>On peut observer que les valeurs lues sont contenues entre 0 et 1024 alors qu'on est en train d'observer une tension qui varie entre 0 et 5V. L'explication est très simple. La carte Arduino (de même que tous les microprocesseur, microcontrôleur, ordinateurs...) ne peut traiter que du binaire. Elle doit donc coder les valeurs analogiques en binaire. Et elle le fait sur 10 bits. Si on calcul 2^10 on obteint 1024. La valeur analogique maximale (5V ici) vaut donc 11 1111 1111 1111 en binaire pour la carte, c'est-à-dire 1024. On sait donc que 1024 corresond à 5V. En appliquant la formule valeur_lue*1024/5 = valeur_en_volts (produit en croix) on peut ainsi retrouver toutes les valeurs en Volts.


#### Montage entier

Maintenant que j'ai vérifié que chacun de mes composants fonctionne correctement, je les assemble d'un seul montage pour réaliser mon objectif initial : faire clignoter la LED en fonction de la valeur du potentiomètre.

J'ai réalisé le montange suivant :  

 * Matériel :  
   * LED rouge  
   * potentiomètre  
   * carte Arduino Uno  
 * Branchements :  
   * LED branchée sur le port 7  
   * potentiomètre branché sur le port A0  

![](../images/montage_led_poto1.JPG)
![](../images/schema_montage_led_poto1.JPG)

Pour piloter la LED en fonction du potentiomètre, j'ai regroupé les deux programmes de test précédents. J'ai remplacé la valeur fixe du ```delay()``` par la valeur lue sur le potentiomètre.

J'ai donc écrit le code suivant :

```
/*
 *
 *  File    : prog1_led_poto.ino
 *  Author  : Doriane Galbez
 *  Date    : 11/03/2021
 *  License : This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 *
 *
 * Programme faisant clignoter une LED en fonction de la valeur lue sur un potentiometre.
 *   - led connectee au port 7
 *   - potentiometre connecte au port A0
 *
 *
 */

//definition des noms attribues aux ports
const int POTO = A0;  //potentiometre connecte sur le port A0
const int LED = 7;    //led connectee sur le port 7
int var;

void setup() {
  pinMode(LED,OUTPUT);    //initialisation du port en tant que sortie
  pinMode(POTO,INPUT);    //initialisation du port en tant qu'entree
}

void loop() {
  digitalWrite(LED,HIGH); //allume la led
  var = analogRead(POTO); //lit la valeur du potentiometre
  delay(var);    //attent autant de millisecondes que la valeur du potentiometre
  digitalWrite(LED,LOW);  //eteint la led
  var = analogRead(POTO); //lit la valeur du potentiometre
  delay(var);    //attent autant de millisecondes que la valeur du potentiometre
}
```


Voici le résultat en [vidéo](../images/video_demo_led_poto3.mp4)

<video controls muted>
<source src="../../images/video_demo_led_poto3.mp4" type="video/mp4">
Cette vidéo ne semble pas fonctionner dans votre navigateur.
</video>



### Programme/circuit 2

Ce programme a été écrit en colaboration avec [Kristine Valat](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/kristine.valat/FabZero-Modules/module05/).

L'objectif de ce programme a été de prendre en main le fonctionnement des 4 afficheurs 7-segments (référence : 5641as). C'est un composant que nous n'avoins jamais utilisé, nous avons donc écrit différents programmes en augmentant la difficulté progressivement afin de bien le prendre en main. Dans un premier temps nous avons simplement essayé d'afficher les segments du centre (le segment G). Ensuite, nous avons allumé ces même segments mais uniquement certains afficheurs (un sur deux par exemple). Finalement, nous avons écrit une fonction qui permet de réaliser une petite animation sur les afficheurs.

#### Le composant

Le composant que nous utilisons (référence : 5641as) est un ensemble de 4 afficheurs 7-segments.
Un afficheur 7-segments est, comme sont nom l'indique, un ensemble de 7 segments lumineux et d'un point lumineux (le point est quasiment toujours présent sur ce genre d'afficheur). On utilise principalement les afficheurs 7-segments pour afficher des chiffres. Les différents segments sont désignés par des lettres allant de A à G, en plus de DP pour le point.

Sur le composant que nous utilisons nous avons quatre afficheurs, il y a donc en plus des sept connexions pour les segments, quatre connexion pour séléctionner le ou les afficheurs à manipuler. Ici les afficheurs numéros 1 à 4 sont respectivement nommés digit 1, digit 2, digit 3 et digit 4.

Le schéma ci-dessous représente les connexions du composants :

![](../images/schema_afficheurs_7seg.JPG)


#### Le montage

L'étude de la datasheet ne nous à pas permis de définir comment nous devions programmer les afficheurs. Nous avons alors cherché des codes exemples pour en avoir une meilleur idée. Nous nous sommes inspiré de [ce site](https://create.arduino.cc/projecthub/SAnwandter1/programming-4-digit-7-segment-led-display-2d33f8) pour faire notre programme.

Nous avons réalisé le montange suivant :  

 * Matériel :  
   * 4 afficheurs 7-segments
   * carte Arduino Uno  
 * Branchements :  
   * afficheurs branchés sur les ports 2 à 13

![](../images/schema_montage_7seg1.JPG)


#### Les différents programmes

Pour réaliser les différents programme dont je parlais précédemment, nous en avons en réalité réalisé un seul dans lequel nous avons fait différentes fonctions que nous appelions ou non.

Pour simlpifier notre code nous avons commencé par écrire une fonction qui permet d'éteindre toute la plaque et une pour allumer un segment sur l'un des quatre afficheurs au choix.

Nous avons alors pu faire notre première étape, allumer tous les segments du mileu.

![](../images/montage_7seg1.JPG)

Ensuite, grâce à ces mêmes fonctions nous avons pu faire clignoter ces segments en altenance.

![](../images/montage_7seg2.JPG)

Finalement, nous avons écrit une fonction (_serpent()_) qui réalise l'animation d'un petit serrpent qui traverse les afficheurs. Actuellement, cette fonction n'est pas tout à fait opérationnelle. Tous les afficheurs allument les mêmes segments. En étudiant des exemples sur internet, nous nous sommes aperçu que le problème vient peut-être de l'absence d'un _delay()_ entre les affichages entre deux afficheurs. Nous le vérifierons dès que nous le pourrons avec le matériel.

Voici-ci dessous le code que nous avons écrit :

```
/*
 *  File    : prog2_7seg.ino
 *  Authors : Doriane Galbez et Kristine Valat
 *  Date    : 11/03/2021
 *  License : This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 * -------------------------------------------
 * Programme pilotant 4 afficheurs 7 segments.
 * -------------------------------------------
 *
 * Sur un afficheur 7 segments, les segments sont nommes de la maniere suivante :
 *       A
 *      ---
 *   F |   | B
 *     | G |
 *      ---
 *   E |   | C
 *     |   |
 *      ---  O DP
 *       D
 *
 *
 */


//definition des noms attribues aux ports
const int SEG_A = 12;    //segment correspondant au segment a
const int SEG_B = 8;     //segment correspondant au segment b
const int SEG_C = 4;     //segment correspondant au segment c
const int SEG_D = 5;     //segment correspondant au segment d
const int SEG_E = 6;     //segment correspondant au segment e
const int SEG_F = 11;    //segment correspondant au segment f
const int SEG_G = 3;     //segment correspondant au segment g
const int SEG_DP = 7;    //segment correspondant au segment du point
const int DIGIT_1 = 13;  //7 segment numero 1
const int DIGIT_2 = 10;  //7 segment numero 2
const int DIGIT_3 = 9;   //7 segment numero 3
const int DIGIT_4 = 2;   //7 segment numero 4


void setup() {
  //initialisation des ports
  pinMode(SEG_A,OUTPUT);   //initialisation du port en tant que sortie
  pinMode(SEG_B,OUTPUT);   //initialisation du port en tant que sortie
  pinMode(SEG_C,OUTPUT);   //initialisation du port en tant que sortie
  pinMode(SEG_D,OUTPUT);   //initialisation du port en tant que sortie
  pinMode(SEG_E,OUTPUT);   //initialisation du port en tant que sortie
  pinMode(SEG_F,OUTPUT);   //initialisation du port en tant que sortie
  pinMode(SEG_G,OUTPUT);   //initialisation du port en tant que sortie
  pinMode(DIGIT_1,OUTPUT); //initialisation du port en tant que sortie
  pinMode(DIGIT_2,OUTPUT); //initialisation du port en tant que sortie
  pinMode(DIGIT_3,OUTPUT); //initialisation du port en tant que sortie
  pinMode(DIGIT_4,OUTPUT); //initialisation du port en tant que sortie

}

void loop() {
  /*serpent(500);*/

  //ici, les segments g d'un afficheur sur 2 clignotent avec une periode de 1 seconde
  eteint_tout();  //on eteint tout pour eviter que les segments precedemment allumes restent allumes
  affiche_un_seg(DIGIT_1,SEG_G);  //allume le segment g du 7 segment 1
  affiche_un_seg(DIGIT_3,SEG_G);  //allume le segment g du 7 segment 3
  delay(1000);    //attente de 1000 millisecondes soit 1 seconde
  eteint_tout();  //on eteint tout pour eviter que les segments precedemment allumes restent allumes
  affiche_un_seg(DIGIT_2,SEG_G);  //allume le segment g du 7 segment 2
  affiche_un_seg(DIGIT_4,SEG_G);  //allume le segment g du 7 segment 4
  delay(1000);    //attente de 1000 millisecondes soit 1 seconde

}

/*
 * Remise a zero de tout les segments
 */
void eteint_tout(){
  /* option 1*/
  digitalWrite(DIGIT_1, LOW);
  digitalWrite(DIGIT_2, LOW);
  digitalWrite(DIGIT_3, LOW);
  digitalWrite(DIGIT_4, LOW);

  /* option 2*/
  /*
  digitalWrite(DIGIT_1, HIGH);
  digitalWrite(DIGIT_2, HIGH);
  digitalWrite(DIGIT_3, HIGH);
  digitalWrite(DIGIT_4, HIGH);
  digitalWrite(SEG_A, LOW);
  digitalWrite(SEG_B, LOW);
  digitalWrite(SEG_C, LOW);
  digitalWrite(SEG_D, LOW);
  digitalWrite(SEG_E, LOW);
  digitalWrite(SEG_F, LOW);
  digitalWrite(SEG_G, LOW);
  digitalWrite(SEG_DP, LOW);
  */
}

/*
 * Eteint un segment sur un des afficheur 7 segments
 * - digit : (DIGIT_1,DIGIT_2,DIGIT_3 ou DIGIT_4) afficheur 7 segments qu'on veut utiliser
 * - seg   : (SEG_A,SEG_B,SEG_C,SEG_D,SEG_E,SEG_F,SEG_G ou SEG_DP) segment qu'on veut eteindre
 */
void eteint_un_seg(int digit, int seg){
  digitalWrite(digit, HIGH); //selection du 7 segments
  digitalWrite(seg, LOW);    //eteint un segment du 7 segments
}

/*
 * Allume un segment sur un des afficheur 7 segments
 * - digit : (DIGIT_1,DIGIT_2,DIGIT_3 ou DIGIT_4) afficheur 7 segments qu'on veut utiliser
 * - seg   : (SEG_A,SEG_B,SEG_C,SEG_D,SEG_E,SEG_F,SEG_G ou SEG_DP) segment qu'on veut allumer
 */
void affiche_un_seg(int digit, int seg){
  digitalWrite(digit, HIGH); //selection du 7 segments
  digitalWrite(seg, HIGH);   //allume un segment du 7 segments
}

/*
 * Definit un ensemble d'etapes representant un serpent qui avance sur les segments des afficheurs.
 * etape 1    etape 2    etape 3    etape 4    etape 5    etape 6    etape 7
 *    _         _             _           _          
 *   |         |          _  |        _  |         _           _
 *   |        _|           |_|      _| |_|       _| |_       _| |         _|
 */
void serpent_etape(int etape){
  eteint_tout();  //on eteint tout pour eviter que les segments precedemment allumes restent allumes
  switch(etape){
    default: //tous les 7 segments sont eteints
      break;
    case 1:
      affiche_un_seg(DIGIT_1,SEG_A);
      affiche_un_seg(DIGIT_1,SEG_F);
      affiche_un_seg(DIGIT_1,SEG_E);
      break;
    case 2:
      //7 segments 1
      affiche_un_seg(DIGIT_1,SEG_D);
      //7 segments 2
      affiche_un_seg(DIGIT_2,SEG_A);
      affiche_un_seg(DIGIT_2,SEG_F);
      affiche_un_seg(DIGIT_2,SEG_E);
      break;
    case 3:
      //7 segments 1
      affiche_un_seg(DIGIT_1,SEG_C);
      affiche_un_seg(DIGIT_1,SEG_G);
      //7 segments 2
      affiche_un_seg(DIGIT_2,SEG_D);
      //7 segments 3
      affiche_un_seg(DIGIT_3,SEG_A);
      affiche_un_seg(DIGIT_3,SEG_F);
      affiche_un_seg(DIGIT_3,SEG_E);
      break;
    case 4:
      //7 segments 1
      affiche_un_seg(DIGIT_1,SEG_D);
      //7 segments 2
      affiche_un_seg(DIGIT_2,SEG_C);
      affiche_un_seg(DIGIT_2,SEG_G);
      //7 segments 3
      affiche_un_seg(DIGIT_3,SEG_D);
      //7 segments 4
      affiche_un_seg(DIGIT_4,SEG_A);
      affiche_un_seg(DIGIT_4,SEG_F);
      affiche_un_seg(DIGIT_4,SEG_E);
      break;
    case 5:
      //7 segments 1 : eteint
      //7 segments 2
      affiche_un_seg(DIGIT_2,SEG_D);
      //7 segments 3
      affiche_un_seg(DIGIT_3,SEG_C);
      affiche_un_seg(DIGIT_3,SEG_G);
      //7 segments 4
      affiche_un_seg(DIGIT_4,SEG_D);
      break;
    case 6:
      //7 segments 1 : eteint
      //7 segments 2 : eteint
      //7 segments 3
      affiche_un_seg(DIGIT_3,SEG_D);
      //7 segments 4
      affiche_un_seg(DIGIT_4,SEG_C);
      affiche_un_seg(DIGIT_4,SEG_G);
      break;
    case 7:
      //7 segments 1 : eteint
      //7 segments 2 : eteint
      //7 segments 3 : eteint
      //7 segments 4
      affiche_un_seg(DIGIT_4,SEG_D);
      break;
  }//fin switch
}

/*
 * Fait avancer un serpent dessine sur les afficheurs grace aux etapes de la fonction serpent_etape().
 * La perdiode corresond au temps d'attente entre deux etapes du serpent
 */
void serpent(int periode){
  for (int i=0;i<=8;i++){
    serpent_etape(i);
    delay(periode);
  }
}
```

### Programme/circuit 3

Le but de ce programme est d'afficher la valeur lue sur le potentiomètre sur les 4 afficheurs 7-segments. Ce circuit permet donc d'assembler un composant du ciruit 1 (le potentiomètre) avec le composant du circuit 2 (les 4 afficheurs 7-segments).

La valeur du potentiomètre est un nombre, elle peut donc être sur plus d'un chiffre. Or, les afficheurs ne peuvent afficher qu'un nombre à la fois. J'ai donc écrit deux fonction qui prennent en entrée un nombre et qui affichent les chiffres de ce nombre un à un sur les afficheurs. La première de ces deux fonctions permet d'afficher la valeur entière brute du potentiomètre (allant de 0 à 1024). La seconde permet d'afficher la valeur décimale en Volts (allant de 0 à 5).
En l'absence de matériel chez moi, j'ai testé mes fonctions de convertion pour passer d'un nombre à plusieurs chiffres à l'aide d'un simulateur.

Pour faciliter l'écriture (et la lecture par la suite) du code j'ai écrit une fonction qui permet d'afficher le chiffre que l'on souhaite sur l'afficheur que l'on a précisé.

Pour l'instant l'affichage ne se fait pas correctement. Le problème devrait être résolu lorsque nous auront résolu le problème de l'animation du serpent de la partie précédente.

J'ai réalisé le montange suivant :  

 * Matériel :  
   * 4 afficheurs 7-segments
   * potentiomètre
   * carte Arduino Uno  
 * Branchements :  
   * afficheurs branchés sur les ports 2 à 13
   * potentiomètre branché sur le port A0

![](../images/schema_montage_7seg_poto.JPG)


Jusqu'à présent, j'ai écrit le code suivant :

```
/*
 *  File        : prog2_7seg_poto.ino
 *  Authors     : Doriane Galbez
 *  Contributor : Kristine Valat
 *  Date        : 19/03/2021
 *  License     : This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *
 *
 * ----------------------------------------------------------------------------------------------------
 * Programme affichant successivement la valeur brute d'un potentiometre puis sa convertion en Volts.
 *  - potentiometre connecte sur le port A0
 *  - afficheurs 7 segments connectes sur les ports 2 a 13
 * ----------------------------------------------------------------------------------------------------
 *  
 *
 * Sur un afficheur 7 segments, les segments sont nommes de la maniere suivante :
 *       A
 *      ---
 *   F |   | B
 *     | G |
 *      ---
 *   E |   | C
 *     |   |
 *      ---  O DP
 *       D
 *
 *
 */


//definition des noms attribues aux ports
const int SEG_A = 12;    //segment correspondant au segment a
const int SEG_B = 8;     //segment correspondant au segment b
const int SEG_C = 4;     //segment correspondant au segment c
const int SEG_D = 5;     //segment correspondant au segment d
const int SEG_E = 6;     //segment correspondant au segment e
const int SEG_F = 11;    //segment correspondant au segment f
const int SEG_G = 3;     //segment correspondant au segment g
const int SEG_DP = 7;    //segment correspondant au segment du point
const int DIGIT_1 = 13;  //7 segment numero 1
const int DIGIT_2 = 10;  //7 segment numero 2
const int DIGIT_3 = 9;   //7 segment numero 3
const int DIGIT_4 = 2;   //7 segment numero 4

const int POTO = A0;


void setup() {
  //initialisation des ports
  pinMode(SEG_A,OUTPUT);   //initialisation du port en tant que sortie
  pinMode(SEG_B,OUTPUT);   //initialisation du port en tant que sortie
  pinMode(SEG_C,OUTPUT);   //initialisation du port en tant que sortie
  pinMode(SEG_D,OUTPUT);   //initialisation du port en tant que sortie
  pinMode(SEG_E,OUTPUT);   //initialisation du port en tant que sortie
  pinMode(SEG_F,OUTPUT);   //initialisation du port en tant que sortie
  pinMode(SEG_G,OUTPUT);   //initialisation du port en tant que sortie
  pinMode(DIGIT_1,OUTPUT); //initialisation du port en tant que sortie
  pinMode(DIGIT_2,OUTPUT); //initialisation du port en tant que sortie
  pinMode(DIGIT_3,OUTPUT); //initialisation du port en tant que sortie
  pinMode(DIGIT_4,OUTPUT); //initialisation du port en tant que sortie

  pinMode(POTO,INPUT);    //initialisstion du port en tant qu'entree

}

void loop() {
  affiche_valeur_poto_brute(analogRead(POTO));
  delay(500);    //attente de 500 millisecondes soit 0,5 seconde
  affiche_valeur_poto_volts(analogRead(POTO));
  delay(500);    //attente de 500 millisecondes soit 0,5 seconde

}

/*
 * Remise a zero de tout les segments
 */
void eteint_tout(){
  /* option 1*/
  digitalWrite(DIGIT_1, LOW);
  digitalWrite(DIGIT_2, LOW);
  digitalWrite(DIGIT_3, LOW);
  digitalWrite(DIGIT_4, LOW);

  /* option 2*/
  /*
  digitalWrite(DIGIT_1, HIGH);
  digitalWrite(DIGIT_2, HIGH);
  digitalWrite(DIGIT_3, HIGH);
  digitalWrite(DIGIT_4, HIGH);
  digitalWrite(SEG_A, LOW);
  digitalWrite(SEG_B, LOW);
  digitalWrite(SEG_C, LOW);
  digitalWrite(SEG_D, LOW);
  digitalWrite(SEG_E, LOW);
  digitalWrite(SEG_F, LOW);
  digitalWrite(SEG_G, LOW);
  digitalWrite(SEG_DP, LOW);
  */
}

/*
 * Eteint un des afficheur 7 segments
 * - digit : (DIGIT_1,DIGIT_2,DIGIT_3 ou DIGIT_4) afficheur 7 segments qu'on veut eteindre
 */
void eteint_un_7seg(int digit){
  /*option 1*/
  digitalWrite(digit, LOW); //selection du 7 segments

  /*option 2*/
  /*digitalWrite(digit, LOW); //selection du 7 segments
  digitalWrite(SEG_A, LOW);
  digitalWrite(SEG_B, LOW);
  digitalWrite(SEG_C, LOW);
  digitalWrite(SEG_D, LOW);
  digitalWrite(SEG_E, LOW);
  digitalWrite(SEG_F, LOW);
  digitalWrite(SEG_G, LOW);
  digitalWrite(SEG_DP, LOW);
  */
}

/*
 * Eteint un segment sur un des afficheur 7 segments
 * - digit : (DIGIT_1,DIGIT_2,DIGIT_3 ou DIGIT_4) afficheur 7 segments qu'on veut utiliser
 * - seg   : (SEG_A,SEG_B,SEG_C,SEG_D,SEG_E,SEG_F,SEG_G ou SEG_DP) segment qu'on veut eteindre
 */
void eteint_un_seg(int digit, int seg){
  digitalWrite(digit, HIGH); //selection du 7 segments
  digitalWrite(seg, LOW);    //eteint un segment du 7 segments
}

/*
 * Allume un segment sur un des afficheur 7 segments
 * - digit : (DIGIT_1,DIGIT_2,DIGIT_3 ou DIGIT_4) afficheur 7 segments qu'on veut utiliser
 * - seg   : (SEG_A,SEG_B,SEG_C,SEG_D,SEG_E,SEG_F,SEG_G ou SEG_DP) segment qu'on veut allumer
 */
void affiche_un_seg(int digit, int seg){
  digitalWrite(digit, HIGH); //selection du 7 segments
  digitalWrite(seg, HIGH);   //allume un segment du 7 segments
}


/*
 * Allume les segments d'un des afficheur 7 segments pour former un chiffre
 * - digit   : (DIGIT_1,DIGIT_2,DIGIT_3 ou DIGIT_4) afficheur 7 segments qu'on veut utiliser
 * - chiffre : le chiffre (0,1,2,3,4,5,6,7,8,9)
 */
void convertion_chiffre_en_segment(int digit, int chiffre){
  eteint_un_7seg(digit); //eteint l'afficheur 7 segments concerne pour
  switch (chiffre){
    default:
      break;
    case 0:
      //           _
      //affiche : | |
      //          |_|
      affiche_un_seg(digit,SEG_A);
      affiche_un_seg(digit,SEG_B);
      affiche_un_seg(digit,SEG_C);
      affiche_un_seg(digit,SEG_D);
      affiche_un_seg(digit,SEG_E);
      affiche_un_seg(digit,SEG_F);
      break;
    case 1:
      //            
      //affiche :   |
      //            |
      affiche_un_seg(digit,SEG_B);
      affiche_un_seg(digit,SEG_C);
      break;
    case 2:
      //           _
      //affiche :  _|
      //          |_
      affiche_un_seg(digit,SEG_A);
      affiche_un_seg(digit,SEG_B);
      affiche_un_seg(digit,SEG_G);
      affiche_un_seg(digit,SEG_E);
      affiche_un_seg(digit,SEG_D);
      break;
    case 3:
      //           _
      //affiche :  _|
      //           _|
      affiche_un_seg(digit,SEG_A);
      affiche_un_seg(digit,SEG_B);
      affiche_un_seg(digit,SEG_G);
      affiche_un_seg(digit,SEG_C);
      affiche_un_seg(digit,SEG_D);
      break;
    case 4:
      //            
      //affiche : |_|
      //            |
      affiche_un_seg(digit,SEG_F);
      affiche_un_seg(digit,SEG_G);
      affiche_un_seg(digit,SEG_B);
      affiche_un_seg(digit,SEG_C);
      break;
    case 5:
      //           _
      //affiche : |_
      //           _|
      affiche_un_seg(digit,SEG_A);
      affiche_un_seg(digit,SEG_F);
      affiche_un_seg(digit,SEG_G);
      affiche_un_seg(digit,SEG_C);
      affiche_un_seg(digit,SEG_D);
      break;
    case 6:
      //           _
      //affiche : |_
      //          |_|
      affiche_un_seg(digit,SEG_A);
      affiche_un_seg(digit,SEG_F);
      affiche_un_seg(digit,SEG_G);
      affiche_un_seg(digit,SEG_C);
      affiche_un_seg(digit,SEG_D);
      affiche_un_seg(digit,SEG_E);
      break;
    case 7:
      //           _
      //affiche :   |
      //            |
      affiche_un_seg(digit,SEG_A);
      affiche_un_seg(digit,SEG_B);
      affiche_un_seg(digit,SEG_C);
      break;
    case 8:
      //           _
      //affiche : |_|
      //          |_|
      affiche_un_seg(digit,SEG_A);
      affiche_un_seg(digit,SEG_B);
      affiche_un_seg(digit,SEG_C);
      affiche_un_seg(digit,SEG_D);
      affiche_un_seg(digit,SEG_E);
      affiche_un_seg(digit,SEG_F);
      affiche_un_seg(digit,SEG_G);
      break;
    case 9:
      //           _
      //affiche : |_|
      //           _|
      affiche_un_seg(digit,SEG_A);
      affiche_un_seg(digit,SEG_B);
      affiche_un_seg(digit,SEG_C);
      affiche_un_seg(digit,SEG_D);
      affiche_un_seg(digit,SEG_F);
      affiche_un_seg(digit,SEG_G);
      break;

  }//fin switch
}



/*
 * Affiche sur les 4 afficheurs 7 segments la valeur brute lue sur le potentiometre.
 * La valeur brute est la valeur sans conversion, elle va de 0 à 1024.
 * - val : valeur (brute) lue sur le potentiometre
 */
void affiche_valeur_poto_brute(int val){
  float tmp = 0.0;
  tmp = (float)val/1000;
  convertion_chiffre_en_segment(DIGIT_1, (int)tmp); //extrait les miliers
  tmp = tmp-(int)tmp;
  tmp = tmp*10;
  convertion_chiffre_en_segment(DIGIT_2, (int)tmp); //extrait les centaines
  tmp = tmp-(int)tmp;
  tmp = tmp*10;
  convertion_chiffre_en_segment(DIGIT_3, (int)tmp); //extrait les dizaines
  tmp = tmp-(int)tmp;
  tmp = tmp*10;
  convertion_chiffre_en_segment(DIGIT_4, (int)tmp); //extrait les unites
}


/*
 * Affiche sur les 4 afficheurs 7 segments la valeur convertie en Volts lue sur le potentiometre.
 * La valeur brute maximum est 1024, elle correspond à 5V. Pour trouver la valeur en Volts on peut simplement
 * faire un produit en croix tel que valeur_en_volts=valeur_brute_lue*5/1024.
 * - val : valeur (brute) lue sur le potentiometre
 */
void affiche_valeur_poto_volts(int val){
  float tmp = (float)val*5/1024;  //convertion de la donnee du potentiometre en Volts
  convertion_chiffre_en_segment(DIGIT_1, (int)tmp); //extrait les miliers
  affiche_un_seg(DIGIT_1,SEG_DP);  //affiche la virgule
  tmp = tmp-(int)tmp;
  tmp = tmp*10;
  convertion_chiffre_en_segment(DIGIT_2, (int)tmp); //extrait les centaines
  tmp = tmp-(int)tmp;
  tmp = tmp*10;
  convertion_chiffre_en_segment(DIGIT_3, (int)tmp); //extrait les dizaines
  tmp = tmp-(int)tmp;
  tmp = tmp*10;
  convertion_chiffre_en_segment(DIGIT_4, (int)tmp); //extrait les unites
}
```
