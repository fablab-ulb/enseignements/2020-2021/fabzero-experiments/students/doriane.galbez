# 4. Découpe assistée par ordinateur

Lors de cette semaine, nous avons travaillé avec des machines réalisant des découpes totales ou partielles (pour faire des gravures par exemple). Nous avons travaillé avec une découpeuse laser et une découpeuse vinyle. Chacune de ces deux découpeuses possède des intérêts et des inconvéniants. La découpeuse laser est très efficace et précise mais elle peut bruler les matériaux, créer des fumer dangereuses et ne doit pas être utiliser avec des matériaux réfléchissants types métaux. La découpeuse vinyle est tout à fait adaptée pour des matériaux fins tels que le papier ou le carton, par contre elle l'est moins pour des matériaux plus épais.


## Découpeuse laser
Pour la découpe laser nous avons travaillé avec du papier cartonné.

### Calibration
Pour la calibration, j'ai travaillé avec [Kristine](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/kristine.valat/), [Théo](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/theo.lisart/) et [Ellias](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/ellias.ouberri/).

La découpeuse laser permet de réaliser des découpes plus ou moins profondes. Elle permet donc de soit découper totalement le matériau soit réaliser des gravures. D'un matériau à l'autre les paramètres de découpe ne sont pas les mêmes. Plus un matériau est épais, ou plus on veut découper profondément, et plus il faudra augementer la puissance et diminuer la vitesse. C'est pour cela qu'il est très intéressant de réaliser une grille de calibration pour tester différentes puissances et vitesses et ainsi connaître les paramètres qui vont nous intéresser pour notre impression.

Le modèle pour la calibration est montré sur la page de [Théo](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/theo.lisart/#FabZero-Modules/module04/).

![](../images/calibration_decoupe_laser.svg)

Après avoir découper le fichier de calibration, nous avons conclu que pour graver le papier cartonné il fallait une vitesse à 100% et une puissance à 5%. Pour découper totalement la feuille, une vitesse à 90% avec une puissance à 20% sont largement suffisants.

### Kirigami
Maintenant que je connais les paramètres à utiliser pour découper la feuille, je peux réaliser un kirigami. Un kirigami un objet réalisé à partir de pliures (comme un origami sauf que l'origami n'autorise pas les découpes). Comme modèle, j'ai utilisé le deuxième kirigami présenté sur [cette page](https://royalsocietypublishing.org/doi/full/10.1098/rspa.2015.0235).

J'ai utilisé Inkskape pour faire le modèle ci-dessous.

![](../images/kirigami1_rogne.svg)

Sur l'ordinateur connecté à la découpeurse laser, j'ai ouvert mon fichier svg sous Inkskape puis j'ai été dans _File->Imprimer_. Dans la fenêtre qui s'est ouverte j'ai sélectionné **Vector** en haut à droite pour régler les paramètres de chaques couleurs. Dans mon cas je veux que les traits noirs soit totalement découpés et que les traits rouges soit juste creusés (cela me permettra de plier facilement la feuille ensuite).

_Remarque importante : pour que les lignes soient prisent en compte lorsqu'on va dans **Vector**, il faut que l'épaisseur des traits dans Inkskape soit de 0,01mm._

![](../images/inkskape_kirigami1_epaisseur.PNG)

Je souhaite que les traits rouges ne soient pas totalement découpés, en paramètre je mets donc la vitesse à 100% et la puissance à 5% pour la couleur rouge.

![](../images/logiciel_decoupe_laser_contours_rouge.PNG)

Je souhaite que les traits noirs soient totalement découpés, en paramètre je mets donc la vitesse à 90% et la puissance à 20% pour la couleur noir.

![](../images/logiciel_decoupe_laser_contours_noir.PNG)

Pour éviter que la feuille ne bouge pendant la découpe il faut mieux faire les découpes profondes en dernier. Ici, il faut donc placer la couleur rouge au-dessus de la noir. En effet, la découpe se fait couleur par couleur en commançant par la couleur qu'on aura placé en haut de la pile. Pour changer l'ordre de la pile dans l'onglet à droite, il suffit de faire glisser le calque que vous voulez au-dessus (ou en-dessous) des autres.

Après la découpe j'ai obtenu la feuille suivante :

![](../images/photo_kirigami1_non_plie.JPG)

J'ai ensuite utilisé les gravures pour plier mon kirigami et j'ai obtenu le résultat suivant :

![](../images/photo_kirigami1_plie.JPG)

[Téléchargez le fichier svg](../images/kirigami1.svg) ou retrouvez le sur [mon dépôt git](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/doriane.galbez/-/blob/master/docs/images/kirigami1.svg).


## Découpeuse vinyle
### Calibration
Comme pour la découpeuse laser, il faut entrer les paramètres corrects dans le logiciel pour obtenir la découpe que nous souhaitons. Pour cela, j'ai dessiner un petit carré (1x1cm) dans le logiciel et j'ai lancé la découpe. Sur la découpeuse Silouhette on peut modifier le point de départ de la découpe en déplaçant la lame grâce aux flèches directionnelles situées sur le côté droit de l'appareil. Cela permet de choisir l'emplacement de la découpe sur la feuille que l'on a insérée dans la machine. J'ai donc découpé plusieurs carrés en modifiant légèrement les paramètres pour connaître les valeur de la puissance et de vitesse qui conviennent le mieux à ce que je voulais découper.


### Découpe d'un sticker
Avec la découpeuse vinyle j'ai voulu voir s'il était possible de découper un logo que j'ai designé il y a quelque temps pour un de mes clubs.

![](../images/logo_strat.JPG)

La première étape a été d'isoler les contours de mon images et d'en faire un fichier au format adéquat pour la découpeuse Silouette. Pour cela, j'ai ouvert Inkskape où j'ai importé mon image. J'ai ensuite sélectionné mon image et j'ai fait _clic droit_ pour choisir _Vectoriser un objet matriciel_. Dans la fenêtre qui s'est ouverte j'ai sélectionné _Détection des contours_ dans le menu déroulant et j'ai cliqué sur _mettre à jour_ en bas pour voir l'aperçu. Puis j'ai cliqué sur _Valider_.

![](../images/logo_strat_vectorisation.JPG)

J'ai en suite ajouter deux cercles pour faire les contours (au final je me suis rendue compte qu'un seul cercle suffisait). J'ai obtenu l'image suivante :

![](../images/image_logo_strat.JPG)

Pour utiliser la découpeuse Silouette, j'ai télécharger le logiciel associé : [Silouette Stuido](https://www.silhouetteamerica.com/software).
Sur Silouette Studio, j'ai importé mon image au format **dxf**.

![](../images/silouhette_studio_home.JPG)

J'ai ensuite été dans l'onglet _ENVOYER_ en haut à droite. De là j'ai choisi le type de matériau que je souhaitais utiliser : "Papier adhésif, Blanc" dans mon cas.

![](../images/silouhette_studio_envoyer2.JPG)

Les paramètres par défaut permettent de découper la partie adhésive sans découper la partie support blanche. Je n'ai donc pas eu à modifier les paramètres (20 en vitesse et 4 en puissance).

Par rapport à mon image de base j'ai obtenu un sticker en plusieurs parties. J'en ai profité pour customiser ma calculatrice.

![](../images/photo_sticker_decoupe_vinyle1.jpg)
![](../images/photo_sticker_decoupe_vinyle2.jpg)

Ci-dessous vous pouvez télécharger les fichiers du logo ou les retrouver sur mon dépôt git :

* [fichier dxf](../decoupe_vinyle/decoupe_vinile_logo_strat1.dxf) / [sur git](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/doriane.galbez/-/blob/master/docs/decoupe_vinyle/decoupe_vinile_logo_strat1.dxf)
* [fichier stuido3](../decoupe_vinyle/decoupe_vinile_logo_strat1.studio3) / [sur git](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/doriane.galbez/-/blob/master/docs/decoupe_vinyle/decoupe_vinile_logo_strat1.studio3)


### Découpe à partir d'un objet paramétrable

Il est possible de réaliser une découpe à partir d'un fichier svg. Avec OpenSCAD il est possible de créer des objets paramétrable puis de les exporter en fichier svg. J'ai décider de créer un fichier OpenSCAD et de l'exporter en svg pour obtenir un sticker paramétrable. J'ai créé un objet représentant une toile d'araignée dans lequel il est possible de régler le nombre de cercles et de lignes de la toile et où il est également possible de définir le rayon du premier cercle.

Depuis OpenSCAD, pour obtenir un fichier svg, il faut utiliser la fonction ```projection()```, calculer le rendu (_F6_) et enfin aller dans _Fichier->Exporter_ et sélectionner _Exporter comme SVG_.

Sur l'image ci-dessous, la toile à 8 lignes et 3 cercle :

![](../images/spider_web_openSCAD.JPG)

J'ai écrit le code suivant :
```
/*
    File    : spider_web.scad
    Author  : Doriane Galbez
    Date    : 29/03/2021
    License : This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

*/

n_lignes = 8;  //nombre de lignes de la toile
$fn = n_lignes;
n_cercle = 3;  //nombre de cercle de la toile
rayon_i = 4;   //taille du premier rayon
espace = rayon_i;    //espace entre chaque cercle
hauteur = 2;   //hauteur de la figure
largeur = 0.5; //largeur des traits
epsilon = 0.1;  

//longueur des lignes de la toile
longueur = rayon_i+(n_cercle-1)*espace+2;

projection()  //projete l'ombre de la piece pour creer un objet 2D
union(){
    //lignes de la toile
    for (i=[0:n_lignes]){
        rotate([0,0,i*360/n_lignes])
        translate([0,-largeur/2,0])
        cube([longueur,largeur,hauteur]);
    }

    //cercles de la toile
    for (i=[0:n_cercle-1]){
        difference(){
            cylinder(hauteur,rayon_i+espace*i,rayon_i+espace*i);
            translate([0,0,-epsilon/2])
            cylinder(hauteur+epsilon,rayon_i+espace*i-largeur,rayon_i+espace*i-largeur);
        }
    }
}
```

Ensuite, depuis Inksake j'ai extrait les contours de mon objet. Pour cela, après avoir ouvert mon fichier svg, j'ai été dans l'onglet _Chemin_ (en ayant de sélectionné mon objet) et j'ai sélectionné _Union_.

![](../images/spider_web_inkskape.JPG)

Puis, j'ai enregistré mon fichier sous un fichier dxf.

Enfin, il ne me restait plus qu'à ouvrir mon fichier dxf dans Silouette Studio et à régler les paramètres comme dans la section précédente pour découper le sticker.

![](../images/spider_web_silouette_studio.JPG)



Ci-dessous vous pouvez télécharger les fichiers de la toile d'araignée ou les retrouver sur mon dépôt git :

* [fichier svg](../decoupe_vinyle/spider_web_l8_c3_r4.svg) / [sur git](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/doriane.galbez/-/blob/master/docs/decoupe_vinyle/spider_web_l8_c3_r4.dxf)
* [fichier dxf](../decoupe_vinyle/spider_web_l8_c3_r4.dxf) / [sur git](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/doriane.galbez/-/blob/master/docs/decoupe_vinyle/spider_web_l8_c3_r4.svg)
