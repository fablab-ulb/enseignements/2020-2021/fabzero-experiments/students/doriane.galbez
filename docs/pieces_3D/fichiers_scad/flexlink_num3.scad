/*
    File    : flexlinks_num3.scad
    Author  : Doriane Galbez
    Date    : 19/02/2021
    License : This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

*/


$fn = 100;
th = 0.6;         //epaisseur
width = 0.7;      //largeur
ext_r = width/2;  //rayon des cylindres exterieurs
diff = 0.1;       //epaisseur des bordures des parties 1 et 2
epsilon = 0.01;   //valeur permettant de realiser des creux legerement plus profond pour obtenir un affichage plus clair



//dimensions pour les trous
dist_r = 0.8; //distance entre les trous
N = 3;      //nombre de trous

distance = 2;  //distance entre les deux parties

//dimensions du fil
length_f = sqrt(pow(distance,2)+pow((dist_r*(N-1)),2));
width_f = 0.1;
theta = asin(distance/sqrt(pow(distance,2)+pow((dist_r*(N-1)),2)));


translate([ext_r,-width,0])
union(){

//premiere partie
translate([0,ext_r,0])
difference(){
    union(){
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
        translate([0,-ext_r,0]) 
        cube([dist_r*(N-1),width,th]);
        translate([dist_r*(N-1),0,0])    //decalage a l'extremite du pave
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
    }
    union(){  //ensemble de N cylindres pour realiser les trous
        for (i=[0:N]){
            translate([dist_r*i,0,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff);
        }
    }
}

//fil
translate([-width_f/2,0,0])
rotate([0,0,-theta])  //decalage a l'extremite de la partie precedente
cube([length_f+width_f,width_f,th]);


//deuxieme partie
translate([0,-distance-ext_r,0])
difference(){
    union(){
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
        translate([0,-ext_r,0]) 
        cube([dist_r*(N-1),width,th]);
        translate([dist_r*(N-1),0,0])    //decalage a l'extremite du pave
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
    }
    union(){  //ensemble de N cylindres pour realiser les trous
        for (i=[0:N]){
            translate([dist_r*i,0,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff);
        }
    }
}

}
