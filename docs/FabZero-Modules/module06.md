# 6. Electronique 2 - Fabrication

Lors de cette semaine, nous avons continuer le prototypage electronique en assemblant un circuit reproduisant le principe de la carte Arduino que nous avions utilisé la semaine dernière. Sur cette carte nous pourrons connecter divers composants et enregistrer des programmes. Les codes doivent toutefois être assez court car la carte ne possèdera pas beaucoup d'espace mémoire.

Pour réaliser la carte, nous avons réaliser les soudures de ses composants sur le circuit réalisé par [Nicolas De Coster](http://fab.academany.org/2018/labs/fablabulb/students/nicolas-decoster/) en suivant [ses instructions](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/electronics/-/blob/dd760ea3b49b74d278b1eec1e4be9e5a45a986a6/Make-Your-Own-Arduino.md).

## La soudure

En électronique, lorsqu'on fait des prototypes on est souvent amené à faire de la soudure. La soudure permmet de connecter des composants sur un circuit. Ici, nous allons souder sur un circuit imprimé (aussi appelé PCB, Printed Circuit Board). Sur un circuit imprimé, les fils que l'on avait utilisé dans le module précédent sont représenté par des pistes ici cuivrée. Les parties cuivrées conduisent le courant électrique. Chaque fil est séparé du reste par l'absence de cuivre.  

![](../images/pcb_pistes.png)

Sur l'image ci-dessus, j'ai représenté en bleu les pistes (les fils entre les composants). En violet j'ai représenté la masse (le GND).

Pour connecter les composants avec les pistes on réalise de la soudure. Pour cela on fait fondre du métal (en général de l'étain) de sorte à ce qu'il soit en contacte avec la piste et le composant. Le métal qu'on utilise en soudure est conducteur, il faut donc faire attention de ne pas relier deux piste qui ne doivent pas l'être en réalisant la soudure. Aussi une belle soudure est une soudure brillante et assez plate. Il faut éviter de faire des grosses boules de métal, cela pourrait ajouter une résistance dans votre circuit là où on n'en veut pas.

Lorsqu'un composant doit être connecté à un certain nombre d'endroits la petite astuce est de souder deux pattes du composant (deux en diagonal si possible) pour fixer le composant sur la carte. Ainsi, les autres soudures sont plus faciles à réaliser car le composant ne peu plus bouger.

Pour finir, prenez votre temps pour souder et surtout faites attention à ne pas rester sur une même soudure trop longtemps, faites des pauses si une soudure prend trop de temps ! En effet, certains composants peuvent ne pas supporter des températures trop élevées comme le microcontrôleur Atmel SAMD11C que l'on va utiliser. Il est donc important d'éloigner de temps en temps le fer à souder du circuit pour le laisser se refroidir.

>**Conseil important d'utilisation du fer à souder :**
>Pour faire fondre de l'étain, une température de 210°C suffit. Et surtout, lorsque vous l'utilisez, nettoyez **régulièrement** votre fer en frottant la pointe sur une éponge humide. Les soudures sont beaucoup plus facile à faire avec un fer propre ! ;)

## Assemblage

![](../images/photo_pcb_vierge_et_composants.JPG)

#### Soudure du microprocesseur

![](../images/photo_pcb_micropro_seul.JPG)

#### Soudure du régulateur de tension

Le régulateur de tension permet d'alimenter la carte avec une tension de 3,3V. Ce régulateur est important car les ports ISB des ordinateur délivrent une tension de 5V. Le microcontrôleur ne peut pas supporter une tension aussi élevée. Le régulateur de tension permet alors de protéger le microcontrôleur qui forme le coeur de la carte.

![](../images/photo_pcb_micropro_et_regulateur2.JPG)

A partir de là on peut connecter la carte sur un port USB d'un ordinateur et regarder si l'ordinateur la reconnaît. Si c'est le cas tout vas bien la carte et le régulateur semble bien fonctionner. Sous Windows, pour vérifier que ma carte est bien reconnue par mon ordinateur j'ai ouvert le gestionnaire de tâches et j'ai regardé si un port COM était détecté. Si c'est bien le cas, la carte est détecté par l'ordinateur.

![](../images/gestionnaire_de_taches.JPG)

#### Soudure de la led

Avec la led il faut associer une résistance. En effet, une leds n'a besoin que d'une tension faible pour s'allumer et une tension trop élevée la ferait griller. Cette led verte est donc accompagnée d'une résistance de 330 Ohms.

**Attention au sens de la LED !** Il faut souder la LED dans le bon sens, sinon elle ne s'allumera pas. Pour connaître le sens de la LED on peut utiliser un multimètre. Pour cela il faut placer le curseur du multimètre sur le symbole de la LED ( ![](../images/symbole_led.png) ) et placer les fils rouge et noir du multimètre aux extrémités de la LED. Si la LED s'allume elle est dans le bon sens (sinon c'est le sens inverse). Dans ce cas le côté du fil noir correspond au côté où la LED doit être connectée à la masse (GND).

![](../images/schema_sens_led.JPG)

![](../images/photo_pcb_led1.JPG)

Maintenant que nous avons une LED, nous pouvons réaliser un petit programme pour la tester. Cela nous permettra de vérifier que tout fonctionne correctement. Pour cela, un programme simple faisant clignoter la LED suffit. J'ai mis mon programme dans la partie suivante ("Programmes de tests").
Voici le résultat en [vidéo](../images/video_test_led.mp4).

<video controls muted>
<source src="../../images/video_test_led.mp4" type="video/mp4">
Cette vidéo ne semble pas fonctionner dans votre navigateur.
</video>

#### Soudure des dernier composants

Pour compléter la carte, j'ai souder une led rouge (associée avec une résistance de 330 Ohms), un bouton poussoire, une résistnace de 1 kOhms et des connecteurs. La led rouge s'allume dès que la carte est branchée. C'est une led de puissance. Elle permet de s'avoir que la carte est bien alimentée. Les connecteur vont permettre de pouvoir venir connecter des composants extérieurs sur la carte.

![](../images/photo_pcb_complet.JPG)

Comme on peut le voir sur la photo précédente, j'ai ajouté du métal sur le connecteur USB. Cela permet d'éviter que le cuivre ne soit peut à peu retiré à chaque fois que je branche la carte sur mon pc. Si le cuivre se retire, je ne pourrais plus utiliser la carte car le courant ne pourrait passer et je ne pourrais plus utiliser ma carte.

![](../images/photo_pcb_sur_pc.JPG)

Pour vérifier que l'ensemble de ma carte fonctionne bien j'ai écrit un programme qui utilise la valeur du bouton pour allumer ou non la LED. Les résultats sont présentés dans la partie suivante.


## Programmes de tests

La programmation de notre carte se fait exactement comme pour la carte Arduino Uno. La seul différence est la carte que nous allons choisir dans l'onglet _Outils_ de l'IDE Arduino. Pour les détails de l'IDE Arduino et les méthodologies sur la programmation, je vous renvoie à ma page sur le [module précédent](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/doriane.galbez/FabZero-Modules/module05/).

### Sélection de la carte

Contrairement à la carte Arduino Uno, notre carte ne fait pas partie des cartes par défaut présentes dans l'IDE. Il faut donc réaliser quelques manipulations pour l'ajouter afin de pouvoir la sélectionner.

Tout d'abords, allez dans _Fichier->Préférences_ et ajoutez l'URL "https://www.mattairtech.com/software/arduino/package_MattairTech_index.json".

![](../images/ide_arduino_preferences.JPG)

Allez ensuite dans _Outils->Type de carte->Gestionnaire de cartes_ et tappez "mattair" dans la barre de recherche. Cliquez sur "Installer".

![](../images/ide_arduino_gestionnaire_cartes.JPG)

Vous avez maintenant accès à la carte que nous sommes en train d'utiliser !

Il faut ensuite choisir les paramètres suivants dans l'onglet _Outils_ :
* ```Type de carte``` : Generic D11C14A
* ```Clock Source``` : INTERNAL_USB_CALIBRATED_OSCILLATOR
* ```USB CONFIG``` : CDC_ONLY
* ```SERIAL_CONFIG``` : ONE_UART_ONE_WIRE_NO_SPI
* ```Bootloader Size``` : 4kB Bootloader (Important ! Sinon, votre code n'a aucune chance de fonctionner car les lignes de code seront erronées).

Laissez les autres paramètres à leurs valeurs par défaut.

Et voilà, vous pouvez maintenant téléverser votre code sur votre carte !


### Emplacement des ports sur la carte

Le tableau ci-dessous indique l'emplacement des ports sur la carte. Il met en lien les pins du microcontrôleur de notre carte avec l'équivalence en Arduino. Il permet de voir que la LED est connectée sur le port 15 et le bouton sur le port 8.

| Arduino | alt | Pin n  |    | Pin n  | alt | Function |
|:-------- | --- | :------ |--- | ------:| --- |--------:|
| 05 | RX | 01 | | 01 | | **5V** ! |
| 08 | Button | 02 | | 02 | | 04 |
| 09 |  | 03 | | 03 | | 02 |
| 14 |  | 04 | | 04 | | 3.3V |
| 15 | LED | 05 | | 05 | | GND |
| 30 |  | 06 | | 06 | | 31 |
| | | | USB CONN| | | |


### Test de la led

Ce programme permet de faire clignoter la LED avec une fréquence de 1 seconde. Comme dit précédemment, la LED est connectée sur le port 15.

```
/*
 * Programme faisant clignoter la led avec une frequence de 1s.
 *
 *  File    : test_pcb_led.ino
 *  Authors : Doriane Galbez
 *  Date    : 18/03/2021
 *  License : This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *  
 */

#define LED 15


void setup() {
  pinMode(LED, OUTPUT); //initialise la led en sortie
}


void loop() {
  digitalWrite(LED, HIGH);   // allume la led
  delay(1000);               // attend 1 seconde
  digitalWrite(LED, LOW);    // eteind la led
  delay(1000);               // attend 1 seconde
}
```

![](../images/video_test_led.mp4)


### Test de la LED avec le bouton

L'obectif de ce programme est de tester le bouton en l'utilisant pour piloter la LED. Les LED est connectée sur le port 15 et le bouton sur le port 8. Si le bouton est appuyé (valeur lue 0) alors la LED s'allume, sinon la LED s'éteint.

```
/*
 * Programme allumant la led lorsque le bouton est appuye.
 *
 *  File    : test_pcb_led.ino
 *  Authors : Doriane Galbez
 *  Date    : 20/03/2021
 *  License : This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
 *  
 */
#define LED 15
#define BOUT 8

void setup() {
  pinMode(LED, OUTPUT); //initialise la led en sortie
  pinMode(BOUT, INPUT); //initialise le bouton en entree
}


void loop() {
  if(digitalRead(BOUT)==0)     //lit la valeur du bouton (0 ou 1)
    digitalWrite(LED, HIGH);   // allume la led
  else
    digitalWrite(LED, LOW);    // eteind la led
}
```

Dans un premier temps, j'ai vu que le bouton fonctionnait bien. Puis, j'ai pu observer que la valeur lu par la carte était extrèmement influencée par l'environnement extérieur. J'ai pu remarquer que la simple approche de ma main vers la carte pouvait provoquer l'allumage de la LED. Comme on peut le voir sur cette [vidéo](../images/video_led_bouton_CEM.mp4).

<video controls muted>
<source src="../../images/video_led_bouton_CEM.mp4" type="video/mp4">
Cette vidéo ne semble pas fonctionner dans votre navigateur.
</video>

En regardant le circuit imprimé de plus près j'ai remarqué que la piste du bouton était aussi relier à un connecteur (les pattes sur lesquels on peut brancher des composants sur la carte). Ces connecteurs ne sont pas reliés à un composant extérieur, ils sont alors comme des "fils en l'air". Or, ce type de fil agit comme une antenne et capte toutes les perturbations extérieurs. Et comme ce fil (donc cette antenne) est relier au port du bouton, je pense que le problème vient de là. Pour vérifier ma théorie j'ai pris une pince métalique (donc conductrice) que j'ai utilisée pour toucher uniquement le connecteur en lien avec le port du bouton. Et la LED s'est allumée ! Je pense que la carte détecte cette pertubation et que cette dernière est suffisante pour que la carte détecte un état bas (lis la valeur 0), ce qui déclenche l'allumage de la LED.

A mon avis pour résoudre le problème des perturbations du bouton, il faudrait corriger le problème de l'antenne causée par le connecteur, probablement en le reliant à quelque chose.
