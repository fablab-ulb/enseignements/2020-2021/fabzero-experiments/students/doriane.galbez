# 3. Impression 3D

Lors de cette semaine, notre objectif a été d'utiliser les imprimantes 3D pour imprimer les pièces que nous avons designé la semaine précédente.


## Outils

Pour imprimer les pièces, j'ai utilisé le logiciel [Prusa Slicer](https://www.prusa3d.com/prusaslicer/).  
Les imprimantes 3D que nous avons a notre disposition sont des Prusa i3 MK3S.


## Impression
### Réglages

Lors d'une impression 3D, il est important de connaître les paramètres de l'imprimante qu'on utilise et également de surveiller l'impression. En effet, toutes les imprimantes ne disposent pas des mêmes dimensions ni des mêmes type de buse.

Il faut commencer par sélectionner la bonne imprimante. Cela se fait dans l'onglet _Réglage de l'imprimante_ où il faut sélectionner l'imprimante voulue, ici "Original Prusa i3 MK3S & MK3S".

![](../images/prusa_slicer_imprimante.JPG)

J'ai ensuite été dans l'onglet _Réglages d'impression_ où j'ai entré les paramètres tels que sur l'image suivante :

![](../images/prusa_slicer_reglage_impression.JPG)

Ensuite, il faut définir la bordure. La bordure permet d'augementer la surface en contact avec le plateau. Dans notre cas elle est particulièrement utile parce que nos pièces sont assez fine. En augmentant la surface en contact avec le plateau nous augmentons l'adhésion de la pièce au plateau. C'est très important car si la pièce se décolle pendant l'impression, cette dernière est ratée et il faut la recommencer depuis le début.
On accède aux paramètres de la bordure dans l'onglet _Jupes et bordures_ de _Réglages d'impression_.

![](../images/prusa_slicer_bordure.JPG)

Pour activer la bordure il faut cocher la case _Bordure_ dans l'onglet _Plateau_.

![](../images/prusa_slicer_plateau_bordure.JPG)

Puis, j'ai été dans l'onglet _Réglages du filament_ pour définir le type du filament que je souhaitais utiliser.  
Il existe plusieurs types de filament : nylon, ABS, PLA, etc... Il faut donc préciser au logiciel quel type de filament on va utiliser. Ici, nous utilisons de la PLA, je choisis donc "Prusment PLA" en haut à gauche de la fenêtre.

![](../images/prusa_slicer_filament.JPG)

Maintenant que les paramètres sont réglés nous pouvons importer les fichiers stl des pièces que nous souhaitons imprimer. (Les fichiers stl sont des fichiers lisible par les logiciels d'impression 3D. Pour créer le fichier stl de votre pièce dans OpenSCAD allez dans _Fichier -> Exporter -> Exporter comme STL_.) Pour cela, j'ai été dans _Fichier -> Importer -> Importer STL/OBJ/AMF/3MF..._.
![](../images/prusa_slicer_importation.png)
Dans mon cas PrusaSlicer semblait détecter que mes fichier stl étaient pouces et m'affichait alors un message me demandant si je voulais convertir mes données, j'ai cliqué sur _Non_.
Ensuite la ou les pièces que j'ai importées s'affichent sur le plateau. _(Remarque : Il peut arriver que les pièces ne s'affichent pas, dans ce cas là il suffit de cliquer sur **Découper** en bas à droite et les pièces apparaîssent.)_

Lorsque j'ai designé mes pièces sur OpenSCAD je ne savais pas quelle était l'unité de mesure, j'ai alors pris le partie de mettre toutes mes valeurs en centimètre. En important mes pièces sur PrusaSlicer j'ai vu que l'unité de mesure était en fait en millimètre. Par exemple, pour l'épaisseur de ma pièce j'ai obtenu 0,6 mm sur l'axe Z alors que je souhaitais avoir 6 mm. Il s'agissait donc simplement d'un problème d'échelle. Ce dernier est très simple à résoudre sur PrusaSlicer. Dans la partie **Manipulation de l'Objet** dans l'onglet _Plateau_, il suffit de figer les proportions en fermant le cadenas (cela permet de s'assurer que les proportions de la pièce soient respectées et de ne modifier la valeur que sur un seul axe car les autres suivront automatiquement) et d'ensuite modifier la valeur de l'échelle en la faisant passer de 100% à 1000% dans mon cas.

![](../images/prusa_slicer_echelle.JPG)

Enfin, je place les pièces sur le plateau comme je le souhaite et je clique sur **Découper**. Je peux ensuite exporter le fichier gcode en cliquant sur **Exporter le fichier G code**. Le fichier gcode est le fichier qu'il faut placer sur la carte SD insérée dans l'imprimante 3D. Il contient les instructions compréhensibles par la machine qui lui indiquent les mouvement à effectuer pour imprimer la pièce. Générer le fichier gcode est donc la dernière étape avant de lancer l'impression.

![](../images/prusa_slicer_plateau1.JPG)

Pour finir avec PrusaSlicer, ce dernier permet d'importer plusieurs fichiers stl et de les placer sur le plateau mais de n'en imprimer que certain. Pour cela, faut cliquer sur l'oeil à côté du nom de la pièce pour l'inclure ou non dans la génération du fichier gcode. S'il est n'est pas incluse elle n'apparaît plus sur le plateau, elle est masquée. Par exemple, sur l'image suivante la pièce appelée _flexlink_num2_t2.stl_ est masquée.

![](../images/prusa_slicer_selection_piece.JPG)


### Lancement de l'impression

Maintenant que nous avons le fichier imprimable, nous pouvons lancer l'impression. A partir de là, plus besoin de l'ordinateur, on ne travaille plus qu'avec l'imprimante. Le fichier gcode est a placer sur une carte SD qui sera insérée dans l'imprimante. Dans le menu principal de l'imprimante 3D allez dans _Print from SD card_, puis sélectionnez le fichier que vous voulez imprimer. Il y a en général un petit temps avant de voir la machine commencer à imprimer. C'est tout à fait normal elle préchauffe son plateau et sa buse. Une fois que c'est deux éléments ont atteint la température souhaitée, l'impression commence !

#### Règles d'impression
Attention, il est important de rester devant la machine au début de l'impression pour vérifier qu'elle se lance correctement ! Et par la suite vérifiez régulièrement que l'impression est toujours correcte. Si votre pièce se décolle, elle ne sera plus statique et le filament ne pourra pas se déposer correctement. Dans ce cas, il faut arrèter l'impression tout de suite et la recommencer. De plus, avant de lancer l'impression il faut toujour vérifier que le filament qui est placé est du bon type (si on a dit à l'imprimante que le filament était de la PLA il faut que le filament placé dans l'imprimante soit effectivement de la PLA). Il faut également vérifier que vous avez suffisamment de filament. La machine ne le vérifiera pas. Si il n'y a plus de fil elle continuera d'imprimer mais dans le vide et il vous faudra alors recommencer l'impression depuis le début. Il n'est pas possible de demander à la machine d'imprimer une pièce en ne commançant pas au début !
Soyez vigilant, les problèmes qui surviennent lors de l'impression, s'ils ne sont pas pris très vite, peuvet endommager la machine. C'est pour cela qu'il est important de surveiller l'impression de vos pièces.


## Mécanisme avec les flexlinks

En associant les flexlinks avec des logo, il est possible de réaliser différents mécanismes. Il y en a un certain nombre [ici](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/class/-/blob/3492f57983b21259d80457abc2f78fa86a1e6420/vade-mecum/compliant-mechanisms.md). J'ai été personnellement très intéressée par les mécanismes bistables tels que le switch. J'ai alors essayé d'en imaginer un. J'ai imaginé un scopion qui avance soit ses pinces soit sa queue. (C'est l'éléphant parmi les exemples qui m'a inspirée pour faire un animal.)
J'ai fait plusieurs schémas pour trouver la meilleurs solution qui me permettrait de concrétiser mon idée. Pour faire mes schémas je suis partie d'une pièce centrale et j'ai monté le mécanisme autour. Pour choisir ma pièce centrale j'ai réfléchi à la pièce qui me permettrait de faire le mécanisme biposition. Je me suis inspiré du switch qui utilise le _Fixed-Fixed Beam_. Cependant contrairement au switch, pour mon système j'ai besoin de fixer des pièces sur ma partie centrale. J'ai donc choisi d'utiliser le _Airplane_. J'ai alors construit mon mécanisme autour de cette pièce. J'ai finalement obtenu les schémas suivants.

Position 1 : Les pinces sont en arrière et la queue en avant :
![](../images/schema_scorpion_position1.jpg)

Position 2 : Les pinces sont en avant et la queue en arrière :
![](../images/schema_scorpion_position2.jpg)

Ensuite, j'ai imprimé les pièces dont j'avais besoin et j'ai assemblé mon mécanisme.

![](../images/lego_scorpion_position1.jpg)
![](../images/lego_scorpion_position2.jpg)

Et voici le résultat en [vidéo](../images/Video_flex_links_bq.mp4).

<video controls muted>
<source src="../../images/Video_flex_links_bq.mp4" type="video/mp4">
Cette vidéo ne semble pas fonctionner dans votre navigateur.
</video>




## Comparaison de mes pièces avec d'autres personnes

Pour réaliser mon mécanisme à base de flexlinks je n'ai pas eu besoin d'adapter les pièces d'autres personnes car j'avais déjà créé les pièces dont j'avais besoin. J'ai quand même été voir le travail des autres pour le comparer au miens et voir s'il y avait des améliorations possible dans mon code OpenSCAD.

En cherchant des différence avec mon programme sur le _Fixed-Fixed Beam_ j'ai remarqué que [Paul](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/paul.bryssinck/FabZero-Modules/module02/) avait utilisé des fonctions. Elles lui permettent de créer sa pièce sur une ligne qu'il lui suffit de translater ensuite à l'emplacement qu'il désire. L'utilisation de fonction lui permet d'éviter la répétition de code. Dans mes codes OpenSCAD, il y a une forte redondance lorsque deux parties sont similaire, je peux donc utiliser les fonctions pour corriger ce problème.

Ensuite, pour ma pièce _Fixed-Fixed Beam - Half Circle_ j'avais un peu peur que ma solution plus complexe que nécessaire (je sais qu'il peut m'arriver que pour trouver une solution j'emprunte un chemin complexe alors qu'il y en avait un autre beaucoup plus simple). J'ai alors été voir comment d'autres avaient codé cette pièce. Le _Fixed-Fixed Beam - Half Circle_ a été fait aussi par [Stéphanie](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/stephanie.krins/FabZero-Modules/module02_conception/) et [Floriane](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/floriane.weyer/FabZero-Modules/module02/). Dans leur code j'ai pu voir qu'elles avait utilisé deux cercles et un carré pour faire la tige courbé en utilisant la même méthode que moi. Cela m'a rassuré sur mon programme et sa pertinance.


## Fichiers téléchargeables
Vous pouvez télécharger ci-dessous mes pièces au formats stl. Pour chaque pièces j'ai modifié les paramètres afin d'obtenir différentes longueurs. Mes pièces sont décrites dans le [module02 : Conception Assistée par Ordinateur](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/doriane.galbez/FabZero-Modules/module02/).

* Pièce 1 : Fixed-Slotted Beam - Straight
  * [taille du fil : 4 cm](../pieces_3D/fichiers_stl/flexlink_num1_t4.stl)
  * [taille du fil : 6 cm](../pieces_3D/fichiers_stl/flexlink_num1_t6.stl)
  * [taille du fil : 8 cm](../pieces_3D/fichiers_stl/flexlink_num1_t8.stl)
* Pièce 2 : Fixed-Fixed Beam
  * [taille du fil : 2 cm](../pieces_3D/fichiers_stl/flexlink_num2_t2.stl)
  * [taille du fil : 4 cm](../pieces_3D/fichiers_stl/flexlink_num2_t4.stl)
  * [taille du fil : 6 cm](../pieces_3D/fichiers_stl/flexlink_num2_t6.stl)
* Pièce 3 : Z
  * [épaisseur de la pièce : 3 mm](../pieces_3D/fichiers_stl/flexlink_num3_e1.stl)
  * [épaisseur de la pièce : 6 mm](../pieces_3D/fichiers_stl/flexlink_num3_e2.stl)
* Pièce 5 : Fixed-Fixed Beam - Half Circle
  * [rayon du cercle : 1 cm](../pieces_3D/fichiers_stl/flexlink_num5_t1.stl)
  * [rayon du cercle : 2 cm](../pieces_3D/fichiers_stl/flexlink_num5_t2.stl)
  * [rayon du cercle : 3 cm](../pieces_3D/fichiers_stl/flexlink_num5_t3.stl)
* Pièce 6 : Fixed-Fixed Beam - Initial Curved
  * [rayon du cercle : 1 cm](../pieces_3D/fichiers_stl/flexlink_num6_t1.stl)
  * [rayon du cercle : 2 cm](../pieces_3D/fichiers_stl/flexlink_num6_t2.stl)
  * [rayon du cercle : 3 cm](../pieces_3D/fichiers_stl/flexlink_num6_t3.stl)
* Pièce 7 : Satellite
  * [taille du fil : 2 cm](../pieces_3D/fichiers_stl/flexlink_num7_t2.stl)
  * [taille du fil : 3 cm](../pieces_3D/fichiers_stl/flexlink_num7_t3.stl)
  * [taille du fil : 4 cm](../pieces_3D/fichiers_stl/flexlink_num7_t4.stl)
* Pièce 8 : Airplane
  * [taille des fil : 2 cm](../pieces_3D/fichiers_stl/flexlink_num7_t2.stl)
  * [taille des fil : 3 cm](../pieces_3D/fichiers_stl/flexlink_num7_t3.stl)
  * [taille des fil : 4 cm](../pieces_3D/fichiers_stl/flexlink_num7_t4.stl)


Ci-dessous, vous pouvez télécharger les fichiers gcode :

* [Pièce 2 : Fixed-Fixed Beam](../pieces_3D/fichiers_gcode/GALBEZ_flexlink_num2_t2_0.2mm_PLA_MK3S_12m.gcode)
* [Pièce 8 : Airplane](../pieces_3D/fichiers_gcode/GALBEZ_flexlink_num8_num2_0.2mm_PLA_MK3S_52m.gcode)  


Vous pouvez aussi retrouver mes fichiers pour l'impression 3D sur [mon git](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/doriane.galbez/-/tree/master/docs/pieces_3D).
