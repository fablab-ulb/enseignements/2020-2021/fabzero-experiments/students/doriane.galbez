#Doriane Galbez

<p align="center">
<img src="images/photo_presentation.jpg">
</p>

###Formation
Je suis en quatrième année (équivalent master première année) de l'école ingénieur ESIEE Paris dans la filière Systèmes Electroniques Inteligents (SEI).
Je suis actuellement en échange Erasmus dans l'Université Libre de Bruxelles.
Ma filière me permet de réaliser des projets autour de la conception de circuit électronique aussi bien d'un point de vu matériel que logiciel.


###Centres d'intérêts
Je suis passionnée de danse, de musique et de dessins. Je porte un grand intérêt dans la réalisation de projet aliant l'informatique et l'électronique.


###Activités
Lorsque je ne suis pas en cours j'aime participer à des activités collectives. Ainsi, je suis membre de l'orchestre d'harmonie et de la ville de Noisy-le-Grand dans lesquels je joue de la flûte traversière et du piccolo. Je suis membre de la compagnie de danse [Moustico' Studio](https://www.moustico-studio.com/). Je suis également vice-présidente du club Chorale d'ESIEE Paris.


###Projets réalisés

* Création d'un jeu vidéo où le joueur doit passer de salle en salle pour récupérer un diament
* Création d'un petit robot autonome destiné au combat [_Sumobot_](https://esieespace.fr/category/sumobot-challenge/)
* Création d'un robot autonome ou télécommandé par téléphone pouvant éviter le vide et les obstacles : [Roby, le robot sortie du jeu](https://jdp.esiee.fr/laureats-2019/)
* Réalisation d'un court-métrage
* Création d'une application aidant à apprendre la musique : [Sonn'ata](https://play.google.com/store/apps/details?id=com.sonnata.sonnata&gl=BE)

<p align="center">
<img src="images/Roby_et_sumobots.JPG">
<i> Roby et quatre sumobots <br>(photo prise à la Japan Expo 2019)</i>
</p>
