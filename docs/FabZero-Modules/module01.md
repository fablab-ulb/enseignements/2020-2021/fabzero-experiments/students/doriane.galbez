# 1. Documentation

##Les outils
Pour le premier cours, j'ai installer sur mon ordinateur (sous Windows 10) les logiciels suivants :

* [Atom](https://atom.io/) et le package [markdown-preview-enhanced](https://atom.io/packages/markdown-preview-enhanced)
* [Git](https://gitforwindows.org/)

Atom est un éditeur de texte. Il permet par exemple de visualiser le rendu d'un code tel que le [Markdown](https://en.wikipedia.org/wiki/Markdown). Les pages de ce sites sont écrites en Markdown. J'ai pu apprendre les bases de ce langage en suivant [ce tutoriel](https://www.markdowntutorial.com/) intéractif.  
Enfin, Git est on outil très pratique pour partager un projet et pour garder une trace des ancienne versions des documents.


## Utilisation de Git
### Création d'un clé SSH

Pour pouvoir établir une connexion entre le serveur de Git et mon ordinateur il faut créer une clé SSH.  
Pour cela, j'ai ouvert un terminal Git Bash (terminal auquel j'ai eu accès après avoir télécharger [Git](https://gitforwindows.org/)). Je me suis ensuite placée dans le répertoir _.ssh_ (la création de ce répertoire n'est pas obligatoire mais je la conseille pour une meilleure organisation de vos fichiers).
```
$ cd .ssh
```

Pour créer la clé il suffit de tapper la commande suivante : ssh-keygen. Vous obtiendrez un résultat similaire à celui-ci :
```
$ ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/c/Users/doria/.ssh/id_rsa): git_fab
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in git_fab
Your public key has been saved in git_fab.pub
The key fingerprint is:
SHA256:KzOSEg/4mBzVazwB76umuWYZys+ISULCudaTuf0PgFk doria@LAPTOP-OL4EA4RG
The key's randomart image is:
+---[RSA 3072]----+
|   .             |
|    +            |
|   . E           |
|..o * o          |
|o=oo B  S        |
|+=+++.+  .       |
|*===+.+..        |
|**+o=. +.        |
|==== .....       |
+----[SHA256]-----+
```
_Remarque : Vous ne voyez pas ce que vous tappez sur la ligne "Enter passphrase" ? C'est tout à fait normal ! Les caractères sont simplement invisibles. Tappez votre phrase de passe ou faites simplement "Entrer" si vous ne souhaitez pas en mettre une._

Vous avez maintenant deux nouveaux fichiers (la commande ```ls``` permet d'afficher les fichiers et répertoires) :
```
$ ls
git_fab  git_fab.pub
```

Vous pouvez récupérer votre clé publique SSH avec la commande suivante :
```
$ cat git_fab.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDXlkcVxIiwOXB5gh0rmORh2Jt1tlXCihH44g9xCQh3NWP4BQ/IFSDjhWObniwmz3eHhZ4NcgWw9c1KO4n9KKxA1Nzs/NZ/egl+90XcMvTom5V8XkWDwZJPfeG69F2CehGYotF4zHTBEFxtMk/kkqkNhr02QCLljMCde+sLVoP91c/GSowbjjrCSHdyTgt9EeRsSITAOTD+a5KXhsMuQJfF38RFhVTAmMOj47aldiRrNZoDnu7fvP4olrtswC8y5xmj3HfLZUv8QRUsLYh5VW5NHqgsAEx+SLcDU0VxNYsM/8lQpZ1ND7mRbwnFKhBy7Lok/h/jmIP/KXyDcenZ3EZEu0qLLmtyF2K6k4d5RCpf4BV19Jvdlv9KQ9h/gA2KQbHTEG1dVn7E80v+Kv87IYYFrkRTiyIHxA+TZGr8Mxdu1Ois2+hM4UnLfRCoPndQmyeV9CjescUII2T+1U4lCl5L/GfJEGV0qEmgnPjbtb84PhtqdxzsOPqY3wGhXPNbnVc= doria@LAPTOP-OL4EA4RG
```
Pour ajouter votre clé SSH sur Git il ne vous reste plus qu'à cliquer sur "Ajouter une clé SSH" et copier coller cette ligne. _Attention veillez à bien communiquer votre clé publique ! Ne communiquez jamais votre clé privée. (Votre clé privée permet de vous garantir une communication sécurisée, vous devez être le seul à la posséder)._

Et voilà ! Vous pouvez maintenant établir une connexion avec votre projet Git.

### Cloner le projet

Pour récupérer le projet sur Git il faut le cloner. Cela permet d'obtenir une copie sur votre ordinateur de travail. Pour cela, depuis un terminal de commande Git (le même que précédemment), placez vous dans le répertoire dans lequel vous souhaitez récupérer les fichiers du projet. Utilisez ensuite la commande suivante avec le lien URL de votre projet :
```
$ git clone https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/doriane.galbez.git
Cloning into 'doriane.galbez'...
remote: Enumerating objects: 44, done.
remote: Counting objects: 100% (44/44), done.
remote: Compressing objects: 100% (40/40), done.
remote: Total 44 (delta 19), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (44/44), 207.54 KiB | 1.25 MiB/s, done.
Resolving deltas: 100% (19/19), done.
```
Vous avez maintenant récupérer votre projet sur votre ordinateur.


### Ajouter/modifier les fichiers

Le clonnage vous permet de récupérer les fichiers du projet mais ne permet pas de les modifier directement sur Git. Si vous ouvrez un de vos fichiers et que vous le modifiez vous devrez ensuite entrer la commande suivante :
```
$ git add votre_fichier
```
Si vous voulez ajouter un nouveau fichier a votre projet vous pouvez utiliser la même commande.  
Vous pouvez également utiliser cette commande pour plusieurs fichiers en même temps :
```
$ git add fichier1 fichier2 fichier3
```

Cette commande permet d'indiquer les changement qui ont eu lieu pour inclure vos modifications dans votre projet sur Git.

### Enregistrer les modifications du projet

Lorsque vous avez terminé votre travail sur votre ordinateur vous devez enregistrer vos modifications l'aide de la commande suivante :
```
$ git commit -m "une rapide description"
```
L'option ```-m``` n'est pas obligatoire mais je la conseille pour suivre l'évolution de votre travail.  
Avant de _commit_ vous pouvez vérifier que vous avez bien effectuer la commande ```add``` sur tous les fichiers où c'était nécessaire avec la commande suivante :
```
$ git status
On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        new file:   fichier1
        new file:   fichier2
        modified:   fichier3
```
Finalement pour envoyer vos modifications sur la plateforme vous pouvez utiliser la commande suivante :
```
$ git push origin master
```
