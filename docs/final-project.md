# Final Project


## Introduction, trouver un sujet

Avant toute chose il m'a fallu trouver un sujet. Et cela n'a pas été chose simple !

Pour établir un sujet nous avons procédé en trois étapes :

* _Engage_
* _Investigate_
* _Act_

Pour ma part, le domaine qui m'attire est la robotique. J'aime beaucoup alier l'électronique et l'informatique et je trouve que le domaine de la robotique aide très souvent à coriger des problèmes de la vie courante, à tel point qu'aujourd'hui on ne peut plus s'en passer !

_**Engage** - Find a challenge that motivates you_
Pour moi, la robotique c'est un domaine qui aide les personnes dans leur quotidien. J'aime énormément créer des systèmes qui aident les personnes à réaliser une action/activité qu'elles auraient du mal à faire sans. Je trouve ces projets très valorisant humainement et cela me motive davantage pour les faire. De plus, la robotique est un domaine que je peux observer dans ma vie de tous les jours mais aussi auquel je suis confronté dans la plus part de mes projets dans mon école d'ingénieur.

_**Investigate** - step on the shoulders of the giants not on their toes_
C'est ici que les choses ont commencé à se compliquer pour moi. Dans cette partie nous devions tenter de trouver une question à laquelle nous voulions répondre. La partie précédente doit normalement nous aider à trouver cette question. Cependant, j'ai un peu de mal à bien cerner un besoin **précis** pour en faire une question. Comme question j'avais "Comment aider les personnes qui en ont besoin à surmonter leur handicape ?"

_**Act** - Develop a solution, implement it and get feedback_

Dans cette partie, nous devions trouver une piste de solution pour répondre à notre question.

La question que j'ai établie était un peu vague, je ne savais pas vraiment vers où me diriger...

Puis j'ai entendu la question de [Kristine](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/kristine.valat/final-project/) et j'ai été tout de suite très intéressée. On a alors décidé de former un binôme. Sa première question était "Comment donner envie de cuisiner dans une cuisine minimale". Dans "cuisine minimale" il faut comprendre une cuisine avec un minimum d'outils (pas de four et/ou de micro-onde, peu d'espace de travail, etc.). Avec ce thème on était plutôt orienté vers le public des étudiants car les appartement étudiants sont souvent assez peu équipés en matériel. En pofinant un peu on avait la question suivante "Comment réaliser des plats techniques et variés dans une cuisine minimale". Les étudiants ont souvent tendance à préparer des plats simples et peu équilibrés. Notre objectif est de les aider à cuisiner des plats plus complexes et mieux équilibrés malgré leur petite cuisine.

### Première piste

Nous avons d'abord pensé à nous intéresser au côté matériel. Dans un premier temps, nous avons pensé à imaginer un four démontable ou transformable en un autre objet. L'objectif était de permettre aux étudiants de réaliser des plats qui nécessite un four tout en économisant de la place dans leur appartement. Avant de commencer à rentrer au coeur de ce projet, nous avons fait quelques recherches pour vérifier si notre solution était intéressante. Nous avons trouvé qu'il était possible de réaliser un effet four simplement en superposant deux poels. Le four démontable nous a alors paru être une solution bien trop complexe (et coûteuse pour les étudiants) par rapport à la solution simple des deux poels. Nous avons donc décidé de partir sur une autre idée.

Notre deuxième idée consistait à créer une table haute pliable. L'objectif était de permettre à l'étudiant d'augmenter sa surface de plan de travail (parce qu'ils sont souvent obligés d'utiliser le bureau sur lequel ils travaillent et n'ont pas d'espace dédié où préparer leurs plats). Pour cela, il fallait que cette table soit pliable pour pouvoir être rangée facilement. Nous avons fait des recherches et nous avons vu qu'il existait déjà beaucoup de tables pliables et de pratiquement toutes les tailles. Cette idée ne nous paraissait pas alors très pertinente.

Enfin, nous avons pensé nous concentrer sur les outils de la cuisine, le fouet électrique par exemple. Il est courant de ne pas en avoir dans un appartement étudiant. Nous pensions que c'était un appareil assez cher pour un étudiant et qu'il pouvait être intéressant d'en créer un à bas coût. Cependant, après quelques recherches nous avons vu qu'il en existait déjà à des prix très bas. En créer un nous même n'avait donc pas d'intérêt.

A ce moment-là, on commençait à ne plus avoir beaucoup de nouvelles idées et s'est dit qu'il pouvait être mieux de changer de piste.


### Deuxième piste

Pour aider les étudiants à mieux manger, on s'est concentré sur leur assiette elle-même. En effet, pour manger plus équilibré, une astuce simple consiste à bien répartir la nourriture dans son assiette. Une assiette équilibrée doit être constituée de 50% de légumes, 25% de protéines (animales ou végétales) et 25% de féculents. Pour aider à faire cette répartition nous avons d’abord imaginé un objet type emporte pièce pour délimiter les proportions. Nous en avions fait un premier prototype en 3D.
![](images/projet/proto_assiette_v1.JPG)

Nous aimions bien l’idée mais l’objet n’est pas très attractif. En discutant avec [Gwendoline](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/gwendoline.best/)
et [Hélène](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/helene.bardijn/), nous avons imaginer une assiette en plusieurs parties où chaque partie correspond à un élément. Avec cette solution nous pouvons imaginer diverses formes d’assiette, où chacun pourrait créer l’assemblage qui lui ressemble. Nous avons réalisé un premier prototype (rond) que nous avons imprimé en partie en 3D (à une échelle 0,5 pour avoir une meilleure idée du rendu).


![](images/projet/proto_assiette_v2.JPG)
![](images/projet/assiette1.JPG)
![](images/projet/assiette2.JPG)

Cependant, nous avons fini par laisser tomber cette idée car nous nous étions éloignés de notre public : les étudiants.



### Idée finale
Là on s'est recentré sur notre public : les étudiants.
Pour être bien clair sur ce public nous avons reformulé notre question initiale : **"Comment aider les étudiants à mieux manger ?"**
Nous avons discuter avec plusieurs personnes afin de comprendre leurs habitudes et pourquoi ils ne cuisinaient pas/peu ou pas/peu de repas équilibrés.

Les principaux points qui sont apparus sont :

* Le manque de temps
* Le manque de motivation
* Le coût des courses
* Le gaspillage lié aux quantités (pour payer moins cher il est plus économique de prendre des plus grandes quantités mais il y a alors un risque de gaspillage et parfois les proportions ne sont tout simplement pas adaptées pour une personne seule)
* Le fait de cuisiner seul est parfois démotivant, alors que cuisiner à plusieurs à tendance à donner envie de mieux cuisiner.

Nous avons donc cherché un moyen pour aider les étudiants sur le problème de la motivation pour cuisiner car il regroupe le problème du temps et de la solitude. Pour cela nous avons eu l' idée suivante :

* créer un groupe via un site internet pour rassembler les personnes pour qu’ils puissent cuisiner leurs plats pour toute une semaine à plusieurs.

**C'est sur cette dernière idée que nous avons basée notre projet final** ainsi que sur la communication de ce projet et comment donner envie aux étudiants de rejoindre le groupe de cuisine ainsi que le moyen de s'inscrire dans le groupe.


## Le projet en lui-même

Son petit nom : All for cook!
![](images/projet/logo_all_for_cook.png)

_All for cook !_ est un site proposant des recettes équilibrées et variées. Il permet de mettre en lien plusieurs personnes pour former un groupe se réunissant chaque semaine pour préparer les plats de la semaine. Il est tout à fait possible de faire plusieurs groupe et de ne pas se cantonner à un seul très grand groupe. Dans un premier temps, les membres du groupe se réunissent (de préférence le week-end) pour faire les courses, ensuite ils se retrouve chez un des membres pour cuisiner ensemble les repas de la semaine. Après la séance chacun à un ensemble de plats pour tenir la semaine, plus besion de cuisiner tous les jours, il n'y a qu'à réchauffer les plats ! Le site propose des plats aléatoirement parmi un ensemble de recettes. Initialement, il y a des recettes préenregistrer mais les membres du groupe peuvent ajouter leurs propres recettes. Suite à la proposition de plats du site, ce dernier donne également la liste de courses correspondante. Ainsi, la démarche est simplifiée pour les membres du groupe.

Objectifs :

* gagner du temps dans la semaine
* aider à réduire le budget
* rencontrer des personnes
* aider à se motiver pour cuisiner
* aider à manger plus équilibré

Par manque de temps nous n'avons pas pu développer le site avec toutes les fonctionnalités que nous souhaitions. Cependant, nous avons tout de même créé un prototype du site [ici](https://dorianegalbez.wixsite.com/allforcook).


## Aspect scientifique

Pour l'aspect scientifique, nous avons efffectué un sondage et fait plusieurs recherches sur comment manger sainement ainsi que les recettes pour les biscuits que nous proposeront afin d'en faire les plus sains possibles.

###Sondage

Nous avons effectué un sondage auprès des étudiants et d'anciens étudiants afin d'avoir leur avis ainsi que leurs besoins [lien du questionnaire](https://docs.google.com/forms/d/1P14jox8yGBuVgEPAVeHW_RNoyC5Z7MC6pW3-wbW72o8/). Nous obtenus les résultats suivants:

![](images/projet/sondage.PNG)
![](images/projet/sondage1.PNG)
![](images/projet/sondage2.PNG)
![](images/projet/sondage3.PNG)
![](images/projet/sondage4.PNG)
![](images/projet/sondage5.PNG)
![](images/projet/sondage6.PNG)
![](images/projet/sondage7.PNG)


Nous pouvons remarquer que 38% des étudiants intérrogés vivent seuls (ou vivaient seuls lorsqu'ils étaient étudiants). Nous remarquons aussi que le nombre d'entre eux cuisinent des plats simples (qui sont rarement équilibrés) est le même. De plus, seulement 22% mangent des assiettes équilibrées. 25% des interrogés sont prêts à rejoindre un groupe de cuisine afin de préparer des repas pour la semaine.  
Nous aussi posé la question de la raison pour laquelle ces personnes cuisinent et nous pouvons catégoriser les réponses entre ceux cuisinent et ceux qui ne le font pas/peu :

* Ceux qui ne cuisinent pas/peu
>"Manque de temps et je rentre chez mes parents le week end"
"En rentrant fatigué pas trop le temps"
"J'ai pas le temps et je ne sais pas bien cuisiner j'ai pas l'habitude)."
"Peu de temps"
"Je travaillais en même temps et j'avais le temps de rien faire, donc les pâtes (les moin cher bien entendu) était la facilité"
"Parce que j’ai pas le temps"

>"Je mange au RU en semaine et le week-end j'aime cuisiner mais je commande/fais des trucs très simple par flemme assez souvent"
"car commander c'est la facilité"

**Nous pouvons remarquer 2 raisons pour lesquelles les étudiants ne cuisinent pas le manque de temps et le manque de motivation. Cela coïncide avec les discussions que nous avons eu au préalable avec d'autres étudiants.**

* Ceux qui cuisinent
>"Aucune restauration pas chère aux alentours à cause du covid, impossible d'aller au RU"
"Je n'ai pas beaucoup d'argent pour commander donc j'essaie de cuisiner moi-même."
"Pas les moyens de commander ou de sortir manger"
"C'est moins cher à long terme de cuisiner plutôt que de commander (ou plats préparés)"

>"J'aime cuisiner, c'est moins cher et ça me permet de décompresser"
"faire à manger me détend et cela est beaucoup plus sain que les plats tout fait"
Je cuisine parce que sa me plait et que je peux manger ce que je desir

>Je fais de la musculation donc je me prépare des dingueries culinaires équilibrées et incroyablement délicieuses tous les jours"

>"Rapide et en grande quantité"
"Je fais des grosses quantites pour gagner du temps et avoir de quoi manger pour plusieurs repas."

>"Par flemme. Parfois je préfère sauter un repas plutôt que de faire la vaisselle (ou alors je fais la vaisselle uniquement quand je meurt de faim pour réutiliser une casserole). En revanche quand j'ai habité chez mon copain pendant les un an de confinement c'était beaucoup plus motivant de lui préparer de bons petits plats et de les partager ensemble (d'autant quil a un lave vaisselle )."

**Nous pouvons remarquer plusieurs raisons pour lesquelles les étudiants cuisinent car cela permet coûte moins cher, c'est possible de faire des grandes quantités, cela détend et cela peut leur permettre de manger plus équilibré. De plus, dans le dernier commentaire, nous pouvons remarquer que la personne trouve ca plus motivant de cuisiner pour quelqu'un et de partager le repas avec cette personne**


###Recherches

"Que veut dire bien manger ?" C'est la principale question que nous nous sommes posées durant ce projet. En effet, il est simple de dire qu'il faut bien manger et manger équilibré mais nous avons du mal à savoir pourquoi ou comment le faire. Nous avons trouvé un article de l'université d'Harvard T. H. Chan School of Public Health’s (département nutrition) qui nous explique ce que doit contenir une assiette équilibrée. ([Lien de l'article en anglais](https://www.hsph.harvard.edu/nutritionsource/healthy-eating-plate/) , [Lien de l'article en français](https://www.hsph.harvard.edu/nutritionsource/healthy-eating-plate/translations/french_canada/))

Dans cet article, nous pouvons voir ce qui entendu quand on parle d'assiette saine pour faire un plat équilibré. Nous observons donc que l'assiettes est divisée en 3 parties, la moitié doit être composée de fruits et de légumes, un quart doit être des cérales complètes et le dernier quart doit être des protéines (animales ou végétales). De plus, à cela nous pouvons ajouter des huiles santés pour accompagner le plat ou la cuisson et il faut boire de l'eau, du thé ou du café. Ils nous informent aussi qu'il faut prendre 1 à 2 portions de produits laitiers par jour (c'est-à-dire 1 à 2 verres de lait /yaourts / tranches de fromage).

![](images/projet/assiette_saine.PNG)

Nous avons trouvé sur un site fait par un professionel de santé plus de détail sur les légumes, protéines, ceréales complètes, bonnes graisses, épices et aromatiques et fruits qu'il faut favoriser pour repas équilibré. ([Voici le lien du site si cela vous intéresse](https://toutpourmasante.fr/composer-repas-equilibre/)).

De plus, nous avons effectué des recherches pour les biscuits que nous souhaitions préparer, nous cherchions un biscuit qui serait peu sucré pour que cela soit le plus sain possible car le sucre n'est pas bon pour la santé et peut entraîner de nombreuses maladies telles que diabète de type 2, des maladies cardiovasculaires et certains cancers. [Source ANSES](https://www.anses.fr/fr/content/sucres-dans-l%E2%80%99alimentation)

Il y a beaucoup de risque d'allergies alimentaires donc nous avons essayer de réduire les risques au maximum.

![](images/projet/allergies.PNG)

[Source Le Monde qui a repris les résultats de l'ANSES](https://www.lemonde.fr/planete/article/2018/12/20/qu-est-ce-qu-on-mange-l-assiette-des-francais-decortiquee_5400180_3244.html)

Nous avons évité les recettes avec du lait, des fruits à coques et des arachides.

Nous avons alors trouvé cette [recette](https://www.marmiton.org/recettes/recette_cookies-sans-sucre-sans-beurre-a-basse-calories_347957.aspx) qui permet de faire des biscuits avec les ingrédients suivants:

* farine
* chocolat noir (car moins sucré que le chocolat au lait)
* bicarbonate (nous l'avons remplacé par de la levure puisque l'effet est le même)
* vanille
* oeuf
* crème
* compote (nous avons utilisée une allégée en sucre)


## Inscription dans les mesures du Développement Durable (Sustainable Development)

Nous avond dû inscrire notre projet dans au moins un des objectifs du développement durable des Nations Unies ([disponibles ici](https://sdgs.un.org/)).
Nous notre projet s'incrit dans 2 objectifs :

* Bonne santé et bien-être. En effet, notre projet permettrait aux étudiants de cuisiner des plats plus équilibrés et donc d'être en meilleure santé. De plus, cela permettrait aussi un contact social qui réduirait la solitude pour les étudiants.
* Réduire les inégalité. En effet, les étudiants ont parfois peu de budget, pas/peu d'accès à du matériel pour cuisiner. Cela leur permettrait d'avoir un budget plus adapté pour cuisiner ainsi que du matériel nécessaire pour cuisiner.

![](images/projet/DD_ONU_3.JPG)
![](images/projet/DD_ONU_10.JPG)

## Utiliser un outil du FabLab

### Principe

L'étape la plus délicate dans notre projet c'est de le faire connaître. Nous avons pensé que nous pourrions utiliser un objet concret pour interpeller les gens et leur donner envie de se renseigner sur le projet. Et puisque nous parlons de cuisine, pourquoi ne pas en profiter pour que cet objet soit des biscuits !

Notre idée est de faire des biscuits avec un minimum de sucre, de rendre ce biscuit attractif et de placer à côté une petite pancarte expliquant le principe du site. Les biscuits seront préparés par les membres du groupe lorsqu’ils se réunissent pour cuisiner leurs plats. Pour que ces biscuits soient attractifs nous souhaitons utiliser des décorations qui pourront varier régulièrement. Une idée que nous avons eu est de créer une sorte de petit moule découpable à la laser pour dessiner la forme que l’on veut en chocolat (noire pour avoir moins de sucre et parce que le chocolat noire contient du magnésium qui est bon pour éviter les crampes par exemple). Ainsi, les membres du groupe qui ont accès au FabLab pourront chaque semaine découper une nouvelle forme (suivant leur envie) et les biscuits auront régulièrement de nouvelles décorations. De plus, préparer des biscuits et les décorer pour les partager ensuite peut être assez motivant pour rejoindre le groupe de cuisine. Sur le site nous souhaitons mettre une partie tutoriel pour montrer comment découper son image pour créer le moule.

_Remarque : Les moules peuvent être réutilisés d’une semaine à l’autre il n’y a pas d’obligation de les changer toutes les semaines._

### Nos exemples

Pour commencer et faire les premiers biscuits, nous avons créé deux moules : le premier dessine un smiley et le deuxième le logo du FabLab.

Pour créer le smiley, nous avons utilisé Inkscape pour directement le dessiner. Pour cela, nous avons utilisé les outils présents sur le côté gauche.


![](images/projet/inkscape_smiley1.png)

> Attention : Pour pouvoir découper en vecteur il faut que les contours aient une épaisseur de 0,01 mm.


![](images/projet/inkscape_smiley2.png)


Ensuite, nous avons découpé notre pièce. Pour trouver les paramètres de la découpe que nous devions utiliser pour du plexiglas nous avons utilisé une grille de calibration disponible à côté de la découpeuse laser. Pour faire une découpe (et non juste graver) nous avons choisi de mettre 10% en vitesse, 55% de puissance et 100% en fréquence.

![](images/projet/decoupe_laser_smiley.png)


Pour le logo du FabLab, nous sommes parties d’une image SVG du logo que nous avons ouverte sous Inkscape.

![](images/projet/Fab_Lab_logo.svg)

De cette image nous avons gardé uniquement le logo.

Ensuite, nous avons effacé le fond et définit les contours en noir. Pour cela, nous avons sélectionné l’image puis nous avons pris une couleur du petit bandeau en bas que nous avon fait glissé dans les cases “Fond” et “Contour”. Pour le fond nous avons l’absence de couleur (la case avec la croix rouge) et pour le contour nous avons pris la couleur noir.

![](images/projet/inkscape_fablab_logo_contours1.png)
![](images/projet/inkscape_fablab_logo_contours2.png)

Puis pour la découpe nous avons réalisé la même manipulation que précédemment avec le smiley.

Nous avons finalement obtenu les deux pièces suivantes :

![](images/projet/moules1.JPG)


Finalement, avec ces moules nous avons décoré nos biscuits.
![](images/projet/photo_biscuits.JPG)

Pour aider à diriger vers le site de "All for cook!" nous ajoutons une petite pancarte placée à côté des biscuits. Voici la [pancarte](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/doriane.galbez/-/tree/master/docs/images/projet/Pancarte.pdf).
