# 2. Conception Assistée par Ordinateur

Lors de cette deuxième semaine, notre objectif a été de réaliser un ensemble de pièces compatibles lego sous le modèle des [_flex links_](https://www.compliantmechanisms.byu.edu/flexlinks). Le but est d'utiliser un logiciel de CAO (Conception Assitée par Ordinateur) pour designer quelques pièces pour former un kit compatible lego de _flex links_.  
Pour nous pouvions utiliser [OpenSCAD](https://www.openscad.org/) ou [FreeCAD](https://www.freecadweb.org/). J'ai préférer utiliser OpenSCAD car ce logiciel me semblait plus intuitif pour pouvoir modifier mes pièces en utilisant des paramètres.

## Pièces réalisées

Pour l'ensemble des pièces suivantes j'ai commencer par décrire les contour de la pièce. J'ai ensuite décrit la forme des trous. Finalement, j'ai réaliser une différence de ces deux descriptions (```difference()```). J'ai donc avancer progressivement. Cette une méthode que je conseille. Avancer de cette manière permet d'augmenter progressivement la complexité du programme sans augmenter particulièrement la difficulté de son écriture. Je conseil également de faire des schémas papiers. Cela permet de poser clairement ses idées et d'écrire les valeurs de vos paramètres.

Toutes les pièces que j'ai décrites ont un code permettant de changer les paramètres au début du programme afin de pouvoir modifier les paramètres facilement et créer de nouvelles pièces à partir de celles déjà décrites.

OpenSCAD est un logiciel informatique et réalise donc des approximations pour les nombres décimaux. Cela peut causer de légers bugs d'affichage. En particulier Lorsque l'on fait une différence entre deux parties possèdant une (ou plusieurs) même dimension. Pour corriger ce problème j'ai légèrement agrandi certaines épaisseurs de +epsilon et translaté suivant l'axe z de -espilon/2. Epsilon est une variable que j'ai défini au début de mon code.

### Pièce 1 : Fixed-Slotted Beam - Straight

![](../images/flexlink1.JPG)

J'ai pensé cette pièce en la divisant en trois parties : les deux extrémités et la partie centrale (la partie flexible).  

![](../images/schema_flexlink1.JPG)

La forme des deux parties aux extrémités est donc la même : un cube avec un cylindre à chacune de ses extrémités. Pour ces deux parties, il y a des trous creusés par des cylindres aux extrémités. Le rayon de ces cylindres est égale au rayon des cylindres créant la partie extérieure de la pièce moins la largeur de la bordure que nous souhaitons. Ces cylindres ont donc un rayon tel que **ext_r-diff**. Pour la première partie, la longueur est réglée par un paramètre et le trou est réalisé par un cube en supplément des deux cylindres.Pour la deuxième partie, la longueur est réglée en fonction du nombre de trous précisé en paramètre (_N_). Pour cela j'utilise l'équation : **dist*(N-1)**. Cette partie réagit donc en fonction du nombre de trous en paramètre.

Pour unir les trois partie j'ai décalé le fil suivant l'axe x de la longueur de la première partie et j'ai décalé (toujours suivant l'axe x) la deuxième partie de la longueur de la première partie plus la longueur du fil.

Pour réaliser cette pièce j'ai écrit le code suivant :
```
/*
    File    : flexlinks_num1.scad
    Author  : Doriane Galbez
    Date    : 18/02/2021
    License : This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

*/


$fn = 100;        //nombre de face des cylindres
th = 0.6;         //epaisseur de la pièce
length = 2.3;
width = 0.7;
ext_r = width/2;  //rayon des cylindres
diff = 0.1;       //epaisseur des bordures des parties 1 et 2
epsilon = 0.01;   //valeur permettant de realiser des creux legerement plus profond pour obtenir un affichage plus clair

//dimensions du fil
length_f = 8;
width_f = 0.1;

N = 2; //nombre de trou de la deuxième partie
dist = 0.8; //distance entre les centres de deux troux consecutifs

union(){

//premiere partie
difference(){
    union(){
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
        translate([0,-ext_r,0])
        cube([length-ext_r*2,width,th]);
        translate([length-ext_r*2,0,0])    //decalage a l'extremite du pave
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
    }
    union(){  //meme forme que la precedente mais en plus petit pour creer le creux
        translate([0,0,-epsilon/2])
        cylinder(th+epsilon,ext_r-diff,ext_r-diff);
        translate([0,-ext_r+diff,-epsilon/2])
        cube([length-ext_r*2,width-diff*2,th+epsilon]);
        translate([length-ext_r*2,0,-epsilon/2])
        cylinder(th+epsilon,ext_r-diff,ext_r-diff);
    }
}

//fil
translate([length-ext_r,-width_f/2,0])  //decalage a l'extremite de la partie precedente
cube([length_f,width_f,th]);


//deuxieme partie
translate([length+length_f,0,0])
difference(){
    union(){
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
        translate([0,-ext_r,0])
        cube([dist*(N-1),width,th]);
        translate([dist*(N-1),0,0])    //decalage a l'extremite du pave
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
    }
    union(){  //ensemble de N cylindres pour realiser les trous
        for (i=[0:N]){
            translate([dist*i,0,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff);
        }
    }
}

}
```

### Pièce 2 : Fixed-Fixed Beam

![](../images/flexlink2.JPG)

Cette pièce est pensée exactement comme la précédente. La seule différence est la première partie qui est maintenant identique à la deuxième. Sur cette pièce il est donc également possible de régler la taille du fil et le nombre de trou aux extrémités.

Pour réaliser cette pièce j'ai écrit le code suivant :
```
/*
    File    : flexlinks_num2.scad
    Author  : Doriane Galbez
    Date    : 19/02/2021
    License : This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

*/


$fn = 100;        //nombre de face des cylindres
th = 0.6;         //epaisseur de la pièce
width = 0.7;      //largeur
ext_r = width/2;  //rayon des cylindres exterieurs
diff = 0.1;       //epaisseur des bordures des parties 1 et 2
epsilon = 0.01;   //valeur permettant de realiser des creux legerement plus profond pour obtenir un affichage plus clair

//dimensions du fil
length_f = 5;
width_f = 0.1;

//dimensions pour les trous
dist = 0.8; //distance entre les trous
N = 4;      //nombre de trous

union(){

//premiere partie
difference(){
    union(){
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
        translate([0,-ext_r,0])
        cube([dist*(N-1),width,th]);
        translate([dist*(N-1),0,0])    //decalage a l'extremite du pave
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
    }
    union(){  //ensemble de N cylindres pour realiser les trous
        for (i=[0:N]){
            translate([dist*i,0,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff);
        }
    }
}

//fil
translate([dist*(N-1)+ext_r,-width_f/2,0])  //decalage a l'extremite de la partie precedente
cube([length_f,width_f,th]);


//deuxieme partie
translate([dist*(N-1)+ext_r*2+length_f,0,0])
difference(){
    union(){
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
        translate([0,-ext_r,0])
        cube([dist*(N-1),width,th]);
        translate([dist*(N-1),0,0])    //decalage a l'extremite du pave
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
    }
    union(){  //ensemble de N cylindres pour realiser les trous
        for (i=[0:N]){
            translate([dist*i,0,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff);
        }
    }
}

}
```

### Pièce 3 : Z

![](../images/flexlink3.JPG)

Comme précédemment j'ai pensé cette pièce en troie parties. Simplement, cette fois-ci les deux parties aux extrémités sont l'une au-dessus de l'autre et non plus alignées. De plus, le fil est maintenant en diagonale. Pour mettre le fil en diagonale j'ai utilisé la fonction _rotate()_. La rotation doit se faire autour de l'axe z pour rester dans le plan (x,y). Pour connaître l'angle de rotation j'ai réalisé le schéma suivant :

![](../images/schema_flexlink3.JPG)

On obtient donc les égalités suivantes :  

* h = sqrt(distance^2+L^2), avec L = dist_r*(N-1)  
* theta = arcsin(distance/h)  

Pour réaliser cette pièce j'ai écrit le code suivant :
```
/*
    File    : flexlinks_num3.scad
    Author  : Doriane Galbez
    Date    : 19/02/2021
    License : This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

*/


$fn = 100;        //nombre de face des cylindres
th = 0.6;         //epaisseur de la pièce
width = 0.7;      //largeur
ext_r = width/2;  //rayon des cylindres exterieurs
diff = 0.1;       //epaisseur des bordures des parties 1 et 2
epsilon = 0.01;   //valeur permettant de realiser des creux legerement plus profond pour obtenir un affichage plus clair



//dimensions pour les trous
dist_r = 0.8; //distance entre les trous
N = 3;      //nombre de trous

distance = 2;  //distance entre les deux parties

//dimensions du fil
length_f = sqrt(pow(distance,2)+pow((dist_r*(N-1)),2));
width_f = 0.1;
theta = asin(distance/sqrt(pow(distance,2)+pow((dist_r*(N-1)),2)));


translate([ext_r,-width,0])
union(){

//premiere partie
translate([0,ext_r,0])
difference(){
    union(){
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
        translate([0,-ext_r,0])
        cube([dist_r*(N-1),width,th]);
        translate([dist_r*(N-1),0,0])    //decalage a l'extremite du pave
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
    }
    union(){  //ensemble de N cylindres pour realiser les trous
        for (i=[0:N]){
            translate([dist_r*i,0,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff);
        }
    }
}

//fil
translate([-width_f/2,0,0])
rotate([0,0,-theta])  //decalage a l'extremite de la partie precedente
cube([length_f+width_f,width_f,th]);


//deuxieme partie
translate([0,-distance-ext_r,0])
difference(){
    union(){
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
        translate([0,-ext_r,0])
        cube([dist_r*(N-1),width,th]);
        translate([dist_r*(N-1),0,0])    //decalage a l'extremite du pave
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
    }
    union(){  //ensemble de N cylindres pour realiser les trous
        for (i=[0:N]){
            translate([dist_r*i,0,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff);
        }
    }
}

}
```

### Pièce 4 : Cantilever Beam

![](../images/flexlink4.JPG)

Cette pièce est la même que la pièce 2 à la différence qu'il n'y a pas la deuxième partie à l'extrémité.

Pour réaliser cette pièce j'ai écrit le code suivant :
```
/*
    File    : flexlinks_num4.scad
    Author  : Doriane Galbez
    Date    : 20/02/2021
    License : This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

*/


$fn = 100;        //nombre de face des cylindres
th = 0.6;         //epaisseur de la pièce
width = 0.7;      //largeur
ext_r = width/2;  //rayon des cylindres exterieurs
diff = 0.1;       //epaisseur des bordures des parties 1 et 2
epsilon = 0.01;   //valeur permettant de realiser des creux legerement plus profond pour obtenir un affichage plus clair

//dimensions du fil
length_f = 10;
width_f = 0.1;

//dimensions pour les trous
dist = 0.8; //distance entre les trous
N = 2;      //nombre de trous

union(){

//premiere partie
difference(){
    union(){
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
        translate([0,-ext_r,0])
        cube([dist*(N-1),width,th]);
        translate([dist*(N-1),0,0])    //decalage a l'extremite du pave
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
    }
    union(){  //ensemble de N cylindres pour realiser les trous
        for (i=[0:N]){
            translate([dist*i,0,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff);
        }
    }
}

//fil
translate([dist*(N-1)+ext_r,-width_f/2,0])  //decalage a l'extremite de la partie precedente
cube([length_f,width_f,th]);

}
```

### Pièce 5 : Fixed-Fixed Beam - Half Circle

![](../images/flexlink5.JPG)

Cette pièce est proche de la pièce 3 à l'exception du fil. Ce dernier n'est maintenant plus définit à l'aide d'un cube mais par un ensemble de deux cylindres et un cube. Le premier cylindre permet de définir la bordure extérieure et le second permet ensuite de réaliser un creu. Nous obtenons ainsi un cylindre creu. Pour ne faitre apparaître plus qu'une moitié de ce cylindre j'ai ajouter un cube avec la fonction _difference()_. Ce cube est de la taille de la moitié du cercle.  
La distance entre les deux parties aux extrémités est maintenant réglée par le rayon du cylindre définissant le fil.  

Pour réaliser cette pièce j'ai écrit le code suivant :
```
/*
    File    : flexlinks_num5.scad
    Author  : Doriane Galbez
    Date    : 20/02/2021
    License : This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

*/


$fn = 100;        //nombre de face des cylindres
th = 0.6;         //epaisseur de la pièce
width = 0.7;      //largeur
ext_r = width/2;  //rayon des cylindres exterieurs
diff = 0.1;       //epaisseur des bordures des parties 1 et 2
epsilon = 0.01;   //valeur permettant de realiser des creux legerement plus profond pour obtenir un affichage plus clair

//dimensions du fil (ensemble de 2 cylindres
rayon_f = 1.5;
width_f = 0.1;

//dimensions pour les trous
dist = 0.8; //distance entre les trous
N = 3;      //nombre de trous

union(){

//premiere partie
difference(){
    union(){
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
        translate([0,-ext_r,0])
        cube([dist*(N-1),width,th]);
        translate([dist*(N-1),0,0])    //decalage a l'extremite du pave
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
    }
    union(){  //ensemble de N cylindres pour realiser les trous
        for (i=[0:N]){
            translate([dist*i,0,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff);
        }
    }
}

//fil
translate([dist*(N-1)+ext_r,-rayon_f+width_f/2,0])  //decalage a l'extremite de la partie precedente
difference(){
    cylinder(th,rayon_f,rayon_f);
    translate([0,0,-epsilon/2])
    cylinder(th+epsilon,rayon_f-width_f,rayon_f-width_f);
    translate([-rayon_f,-rayon_f,-epsilon/2])
    cube([rayon_f,rayon_f*2,th+epsilon]);
}


//deuxieme partie
translate([0,-rayon_f*2+width_f,0])
difference(){
    union(){
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
        translate([0,-ext_r,0])
        cube([dist*(N-1),width,th]);
        translate([dist*(N-1),0,0])    //decalage a l'extremite du pave
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
    }
    union(){  //ensemble de N cylindres pour realiser les trous
        for (i=[0:N]){
            translate([dist*i,0,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff);
        }
    }
}

}
```

### Pièce 6 : Fixed-Fixed Beam - Initial Curved

![](../images/flexlink6.JPG)

Cette pièce est la même que la précédente. J'ai ajouté un cube avec la fonction _difference()_ pour effacer un quart du cercle en plus. Le cercle du fil n'est maintenant plus qu'un quart de cercle. J'ai inversé les axes de la deuxième partie pour qu'elle soit orientée vers la verticale et non plus à l'horizontal comme précédemment.  

Pour réaliser cette pièce j'ai écrit le code suivant :
```
/*
    File    : flexlinks_num6.scad
    Author  : Doriane Galbez
    Date    : 20/02/2021
    License : This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

*/


$fn = 100;        //nombre de face des cylindres
th = 0.6;         //epaisseur de la pièce
width = 0.7;      //largeur
ext_r = width/2;  //rayon des cylindres exterieurs
diff = 0.1;       //epaisseur des bordures des parties 1 et 2
epsilon = 0.01;   //valeur permettant de realiser des creux legerement plus profond pour obtenir un affichage plus clair

//dimensions du fil (ensemble de 2 cylindres
rayon_f = 3;
width_f = 0.1;

//dimensions pour les trous
dist = 0.8; //distance entre les trous
N = 2;      //nombre de trous

union(){

//premiere partie
difference(){
    union(){
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
        translate([0,-ext_r,0])
        cube([dist*(N-1),width,th]);
        translate([dist*(N-1),0,0])    //decalage a l'extremite du pave
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
    }
    union(){  //ensemble de N cylindres pour realiser les trous
        for (i=[0:N]){
            translate([dist*i,0,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff);
        }
    }
}

//fil
translate([dist*(N-1)+ext_r,-rayon_f+width_f/2,0])  //decalage a l'extremite de la partie precedente
difference(){
    cylinder(th,rayon_f,rayon_f);
    translate([0,0,-epsilon/2])
    cylinder(th+epsilon,rayon_f-width_f,rayon_f-width_f);
    translate([-rayon_f,-rayon_f,-epsilon/2])
    cube([rayon_f,rayon_f*2,th+epsilon]);
    translate([-epsilon/2,-rayon_f,-epsilon/2])
    cube([rayon_f+epsilon,rayon_f,th+epsilon]);
}

//deuxieme partie
translate([dist*(N-1)+ext_r+rayon_f-width_f/2,-rayon_f-(dist*(N-1)+ext_r)+width_f/2,0])
difference(){
    union(){
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
        translate([-ext_r,0,0])
        cube([width,dist*(N-1),th]);
        translate([0,dist*(N-1),0])    //decalage a l'extremite du pave
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
    }
    union(){  //ensemble de N cylindres pour realiser les trous
        for (i=[0:N]){
            translate([0,dist*i,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff);
        }
    }
}

}
```

### Pièce 7 : Satellite

![](../images/flexlink7.JPG)

Là encore j'ai pensé cette pièce en la divisant en plusieurs parties : les deux parties en angle, la partie centrale et la partie "satellite".
![](../images/schema_flexlink7_1.JPG)
J'ai réalisé un schéma pour définir les formes et valeurs dont j'avais besoin pour chacune des parties.
![](../images/schema_flexlink7_2.JPG)

Maintenant que j'ai chaque partie de ma pièce il faut les placer les unes par rapport aux autres. Précédemment, la partie centrale était décalée suivant x de la longueur de la première partie. Ici, la première partie est inclinée d'un angle theta que j'ai fixé à 45°. La partie centrale doit donc être décalée non plus de la longueur de la première partie (c.à.d **dist*2**) mais d'une distance _x_. En faisant un schéma, on peut voir qu'il est possible de calculer cette distance avce les formules trigonométriques : **x = sin(theta) * dist*2**. De plus, le point de repère de la première partie est situé sur le coin gauche en haut du cube formant cette partie. Pour que cette partie soit corretement alignée sur l'axe des y il faut donc effectuer un déplacement sur une distance _y_ tel que : **y = cos(theta) * dist*2**. J'ai représenté les distances _x_ et _y_ sur le schéma suivant :

![](../images/schema_flexlink7_3.JPG)

Finalement, comme on peut le voir sur le schéma ci-dessus il y a deux distances supplémentaires à prendre en compte pour que les deux parties coïncides parfaitement : _delta_x_ et _delta_y_.Je n'ai pas réussit à trouver de calcul pour définir ces valeurs. Pour les trouver j'ai réalisé un schéma papier à l'échelle et j'ai les ai mesurées avec une règle. J'ai obtenu **delta_x = 0,25 cm** et **delta_y = 0,1 cm**.


Pour réaliser cette pièce j'ai écrit le code suivant :
```
/*
    File    : flexlinks_num7.scad
    Author  : Doriane Galbez
    Date    : 20/02/2021
    License : This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

*/


$fn = 100;        //nombre de face des cylindres
th = 0.6;         //epaisseur de la pièce
width = 0.7;      //largeur
ext_r = width/2;  //rayon des cylindres exterieurs
diff = 0.1;       //epaisseur des bordures des parties 1 et 2
epsilon = 0.01;   //valeur permettant de realiser des creux legerement plus profond pour obtenir un affichage plus clair
theta = 45;       //angle d'inclinaison des parties sur les cotes

//dimensions de la partie centrale
length_c = 1.5;
delta_y = 0.1;
delta_x = 0.25;

//dimension de la partie "satellite"
length_s = 2;
width_s = 0.1;
N_s = 2;

//dimensions pour les trous
dist = 0.8; //distance entre les trous
N = 3;      //nombre de trous

union(){

//premiere partie en angle
translate([0,-(cos(theta)*(dist*(N-1)+ext_r))-delta_y,0])
rotate([0,0,theta])
difference(){
    union(){
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
        translate([0,-ext_r,0])
        cube([dist*(N-1),width,th]);
        translate([dist*(N-1),0,0])    //decalage a l'extremite du pave
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
    }
    union(){  //ensemble de N cylindres pour realiser les trous
        for (i=[0:N]){
            translate([dist*i,0,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff);
        }
    }
}

//partie centrale
translate([sin(theta)*(dist*(N-1)+ext_r)-delta_x,-ext_r,0])  //decalage a l'extremite de la partie precedente
difference(){
    union(){
        cylinder(th,ext_r,ext_r);
        translate([0,-ext_r,0])
        cube([length_c+ext_r*2-diff,width,th]);
        translate([length_c+ext_r*2-diff,0,0]) //decalage a l'extremite du pave
        cylinder(th,ext_r,ext_r);
    }
    union(){ //creux
        translate([0,0,-epsilon/2])
        cylinder(th+epsilon,ext_r-diff,ext_r-diff); //cylindre a une extremite

        translate([ext_r*2-diff/2,0,0])
        union(){
            translate([0,0,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff); //cylindre pour arrondir le bord interieur

            translate([0,-ext_r+diff,-epsilon/2])
            cube([length_c-ext_r*2,width-diff*2,th+epsilon]);

            translate([length_c-ext_r*2,0,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff); //cylindre pour arrondir le bord interieur
        }

        translate([length_c+ext_r*2-diff,0,-epsilon/2])
        cylinder(th+epsilon,ext_r-diff,ext_r-diff); //cynlindre a une extremite
    }
}


//deuxieme partie en angle
translate([sin(theta)*(dist*(N-1)+ext_r)+(length_c+ext_r*2-diff)-delta_x,-ext_r,0])
rotate([0,0,-theta])
difference(){
    union(){
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
        translate([0,-ext_r,0])
        cube([dist*(N-1),width,th]);
        translate([dist*(N-1),0,0])    //decalage a l'extremite du pave
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
    }
    union(){  //ensemble de N cylindres pour realiser les trous
        for (i=[0:N]){
            translate([dist*i,0,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff);
        }
    }
}


//partie "satellite"
translate([sin(theta)*(dist*(N-1)+ext_r)-delta_x+(length_c+ext_r*2-diff)/2+width_s/2,-(dist*(N_s-1)+ext_r+length_s+width),0])
rotate([0,0,90])
union(){
    difference(){
        union(){
            cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
            translate([0,-ext_r,0])
            cube([dist*(N_s-1),width,th]);
            translate([dist*(N_s-1),0,0])    //decalage a l'extremite du pave
            cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
        }
        union(){  //ensemble de N cylindres pour realiser les trous
            for (i=[0:N_s]){
                translate([dist*i,0,-epsilon/2])
                cylinder(th+epsilon,ext_r-diff,ext_r-diff);
            }
        }
        }

        //fil
        translate([dist*(N_s-1)+ext_r,-width_s/2,0])  //decalage a l'extremite de la partie precedente
        cube([length_s,width_s,th]);
    }

}
```

### Pièce 8 : Airplane

![](../images/flexlink8.JPG)

Pour cette pièce, je suis partie de la base de la pièce précédente. J'ai dupliqué la partie satellite que j'ai ensuite placé en haut de la pièce après l'avoir fait tourner suivant l'axe z.

Pour réaliser cette pièce j'ai écrit le code suivant :
```
/*
    File    : flexlinks_num8.scad
    Author  : Doriane Galbez
    Date    : 21/02/2021
    License : This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

*/


$fn = 100;        //nombre de face des cylindres
th = 0.6;         //epaisseur de la pièce
width = 0.7;      //largeur
ext_r = width/2;  //rayon des cylindres exterieurs
diff = 0.1;       //epaisseur des bordures des parties 1 et 2
epsilon = 0.01;   //valeur permettant de realiser des creux legerement plus profond pour obtenir un affichage plus clair
theta = 45;

//dimensions de la partie centrale
length_c = 1.5;
delta_y = 0.1;
delta_x = 0.25;

//dimension de la partie "satellite"
length_s = 2;
width_s = 0.1;
N_s = 2;

//dimensions pour les trous
dist = 0.8; //distance entre les trous
N = 3;      //nombre de trous

union(){

//premiere partie en angle
translate([0,-(cos(theta)*(dist*(N-1)+ext_r))-delta_y,0])
rotate([0,0,theta])
difference(){
    union(){
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
        translate([0,-ext_r,0])
        cube([dist*(N-1),width,th]);
        translate([dist*(N-1),0,0])    //decalage a l'extremite du pave
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
    }
    union(){  //ensemble de N cylindres pour realiser les trous
        for (i=[0:N]){
            translate([dist*i,0,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff);
        }
    }
}

//partie centrale
translate([sin(theta)*(dist*(N-1)+ext_r)-delta_x,-ext_r,0])  //decalage a l'extremite de la partie precedente
difference(){
    union(){
        cylinder(th,ext_r,ext_r);
        translate([0,-ext_r,0])
        cube([length_c+ext_r*2-diff,width,th]);
        translate([length_c+ext_r*2-diff,0,0]) //decalage a l'extremite du pave
        cylinder(th,ext_r,ext_r);
    }
    union(){ //creux
        translate([0,0,-epsilon/2])
        cylinder(th+epsilon,ext_r-diff,ext_r-diff); //cylindre a une extremite

        translate([ext_r*2-diff/2,0,0])
        union(){
            translate([0,0,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff); //cylindre pour arrondir le bord interieur

            translate([0,-ext_r+diff,-epsilon/2])
            cube([length_c-ext_r*2,width-diff*2,th+epsilon]);

            translate([length_c-ext_r*2,0,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff); //cylindre pour arrondir le bord interieur
        }

        translate([length_c+ext_r*2-diff,0,-epsilon/2])
        cylinder(th+epsilon,ext_r-diff,ext_r-diff); //cynlindre a une extremite
    }
}


//deuxieme partie en angle
translate([sin(theta)*(dist*(N-1)+ext_r)+(length_c+ext_r*2-diff)-delta_x,-ext_r,0])
rotate([0,0,-theta])
difference(){
    union(){
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
        translate([0,-ext_r,0])
        cube([dist*(N-1),width,th]);
        translate([dist*(N-1),0,0])    //decalage a l'extremite du pave
        cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
    }
    union(){  //ensemble de N cylindres pour realiser les trous
        for (i=[0:N]){
            translate([dist*i,0,-epsilon/2])
            cylinder(th+epsilon,ext_r-diff,ext_r-diff);
        }
    }
}


//partie "satellite" basse
translate([sin(theta)*(dist*(N-1)+ext_r)-delta_x+(length_c+ext_r*2-diff)/2+width_s/2,-(dist*(N_s-1)+ext_r+length_s+width),0])
rotate([0,0,90])
union(){
    difference(){
        union(){
            cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
            translate([0,-ext_r,0])
            cube([dist*(N_s-1),width,th]);
            translate([dist*(N_s-1),0,0])    //decalage a l'extremite du pave
            cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
        }
        union(){  //ensemble de N cylindres pour realiser les trous
            for (i=[0:N_s]){
                translate([dist*i,0,-epsilon/2])
                cylinder(th+epsilon,ext_r-diff,ext_r-diff);
            }
        }
        }

        //fil
        translate([dist*(N_s-1)+ext_r,-width_s/2,0])  //decalage a l'extremite de la partie precedente
        cube([length_s,width_s,th]);
    }


//partie "satellite" haute
translate([sin(theta)*(dist*(N-1)+ext_r)-delta_x+(length_c+ext_r*2-diff)/2+width_s/2,(dist*(N_s-1)+ext_r+length_s),0])
rotate([0,0,-90])
union(){
    difference(){
        union(){
            cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
            translate([0,-ext_r,0])
            cube([dist*(N_s-1),width,th]);
            translate([dist*(N_s-1),0,0])    //decalage a l'extremite du pave
            cylinder(th,ext_r,ext_r);  //cylindre pour creer l'arrondi
        }
        union(){  //ensemble de N cylindres pour realiser les trous
            for (i=[0:N_s]){
                translate([dist*i,0,-epsilon/2])
                cylinder(th+epsilon,ext_r-diff,ext_r-diff);
            }
        }
    }

    //fil
    translate([dist*(N_s-1)+ext_r,-width_s/2,0])  //decalage a l'extremite de la partie precedente
    cube([length_s,width_s,th]);
}

}
```
